webpackJsonp([8,16],{

/***/ 1108:
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__(0);
var router_1 = __webpack_require__(156);
var login_service_1 = __webpack_require__(1109);
var core_2 = __webpack_require__(0);
var LoginComponent = (function () {
    function LoginComponent(router, LoginService) {
        this.router = router;
        this.LoginService = LoginService;
        this.validationStatus = false;
        this.usernameStatus = false;
        this.passwordStatus = false;
        this.usernameStatusError = false;
        this.passwordStatusError = false;
        this.loginfail = false;
        this.model = "helllllo";
        this.userdata = {};
        this.email = {};
        this.password = {};
        this.succssmsg = false;
        this.popupscreen = false;
        this.passwordsame = false;
        this.router = router;
        var access = window.location.href.split("/");
        console.log(access.includes('token'));
        if (access.includes('token')) {
            this.popupscreen = true;
            console.log(this.popupscreen);
            var token = access[access.length - 1];
            localStorage.setItem("token", token);
        }
        else {
            this.popupscreen = false;
        }
    }
    LoginComponent.prototype.authenticate = function (name, password) {
        var _this = this;
        this.userdata.username = name;
        this.userdata.password = password;
        this.LoginService.login(this.userdata).subscribe(function (data) {
            localStorage.setItem('username', data.username);
            if (data.user_type == "admin") {
                localStorage.setItem('userId', data.user_id);
                localStorage.setItem("lang", "en");
                localStorage.setItem('conceptStatus', data.concept_status);
                localStorage.setItem('stoplist_status', data.stoplist_status);
                localStorage.setItem('usertype', data.user_type);
                localStorage.setItem('auth_token', data.auth_token);
                _this.router.navigate(['/dashboard']);
                //setTimeout(function(){ window.location.reload() }, 300);
            }
            else if (data.user_type == "user") {
                localStorage.setItem('userId', data.user_id);
                localStorage.setItem('counter', "0");
                localStorage.setItem('conceptStatus', data.concept_status);
                localStorage.setItem('stoplist_status', data.stoplist_status);
                localStorage.setItem("backProjectCounter", "0");
                localStorage.setItem('usertype', data.user_type);
                localStorage.setItem('auth_token', data.auth_token);
                _this.router.navigate(['/project']);
            }
            else if (data.user_type == "superuser") {
                localStorage.setItem('userId', data.user_id);
                localStorage.setItem('usertype', data.user_type);
                localStorage.setItem('conceptStatus', data.concept_status);
                localStorage.setItem('stoplist_status', data.stoplist_status);
                localStorage.setItem('auth_token', data.auth_token);
                _this.router.navigate(['/superdashboard']);
            }
            else {
                _this.loginfail = true;
                _this.router.navigate(['/login']);
            }
        });
    };
    LoginComponent.prototype.validateUsername = function (event) {
        this.uusername = event.target.value;
        if (this.uusername) {
            this.usernameStatus = true;
            this.usernameStatusError = false;
        }
        else {
            this.usernameStatus = false;
            this.usernameStatusError = true;
        }
        if (this.usernameStatus && this.passwordStatus) {
            this.validationStatus = true;
        }
        else {
            this.validationStatus = false;
        }
    };
    LoginComponent.prototype.validatePassword = function (event) {
        var pass = event.target.value;
        if (pass) {
            this.validationStatus = true;
            this.passwordStatus = true;
            this.passwordStatusError = false;
        }
        else {
            this.validationStatus = false;
            this.passwordStatus = false;
            this.passwordStatusError = true;
        }
        if (this.usernameStatus && this.passwordStatus) {
            this.validationStatus = true;
        }
        else {
            this.validationStatus = false;
        }
    };
    LoginComponent.prototype.forgotPopUp = function () {
        document.getElementById("forgot").click();
        $('#header').val('');
        $('#body').val('');
    };
    LoginComponent.prototype.checkEmail = function (email) {
        var _this = this;
        this.email.email = email;
        this.LoginService.ValidateEmail(this.email).subscribe(function (data) {
            if (data.succss == "true") {
                _this.succssmsg = true;
                _this.errormsg = false;
                console.log("success msg==", _this.succssmsg);
            }
            else {
                _this.errormsg = true;
                _this.succssmsg = false;
                console.log("error msg==", _this.errormsg);
            }
            console.log(data);
        });
    };
    LoginComponent.prototype.setPassword = function (newpass, confirmpass) {
        var _this = this;
        console.log(newpass);
        if (newpass == "") {
            this.passwordsame = true;
        }
        else if (confirmpass == "") {
            this.passwordsame = true;
        }
        else if (newpass == confirmpass) {
            this.password.password = confirmpass;
            this.LoginService.changepassword(this.password, localStorage.getItem("token")).subscribe(function (data) {
                _this.errormsg = data.succss;
                _this.updatepasswordmg = data.msg;
                console.log(data);
            });
        }
        else {
            this.passwordsame = true;
        }
        console.log(this.passwordsame);
    };
    LoginComponent.prototype.keyDownFunction = function (event) {
        if (event.keyCode == 13) {
            alert('you just clicked enter');
            console.log("you just clicked enter");
        }
    };
    return LoginComponent;
}());
__decorate([
    core_2.Input(),
    __metadata("design:type", Object)
], LoginComponent.prototype, "model", void 0);
LoginComponent = __decorate([
    core_1.Component({
        selector: 'login',
        template: __webpack_require__(1206)
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof router_1.Router !== "undefined" && router_1.Router) === "function" && _a || Object, typeof (_b = typeof login_service_1.LoginService !== "undefined" && login_service_1.LoginService) === "function" && _b || Object])
], LoginComponent);
exports.LoginComponent = LoginComponent;
var _a, _b;
//# sourceMappingURL=/home/admin1/Music/recenceptViewer.com UI/src/login.component.js.map

/***/ }),

/***/ 1109:
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__(0);
var http_1 = __webpack_require__(103);
__webpack_require__(241);
var environment_1 = __webpack_require__(243);
var LoginService = (function () {
    function LoginService(http) {
        this.http = http;
    }
    LoginService.prototype.login = function (user) {
        var headers = new http_1.Headers();
        //headers.append('Content-Type', 'application/json');
        //headers.append('Access-Control-Allow-Origin', '*');
        //let options = new RequestOptions({ headers: headers });
        return this.http.post(environment_1.environment.baseUrl + "login/", JSON.stringify(user))
            .map(function (response) {
            return response.json();
        });
    };
    LoginService.prototype.ValidateEmail = function (email) {
        return this.http.post(environment_1.environment.baseUrl + "forgotpassword/", JSON.stringify(email))
            .map(function (response) {
            return response.json();
        });
    };
    LoginService.prototype.changepassword = function (password, token) {
        return this.http.post(environment_1.environment.baseUrl + "changepassword/" + token + "/", JSON.stringify(password))
            .map(function (response) {
            return response.json();
        });
    };
    return LoginService;
}());
LoginService = __decorate([
    core_1.Injectable(),
    __metadata("design:paramtypes", [typeof (_a = typeof http_1.Http !== "undefined" && http_1.Http) === "function" && _a || Object])
], LoginService);
exports.LoginService = LoginService;
var _a;
//# sourceMappingURL=/home/admin1/Music/recenceptViewer.com UI/src/login.service.js.map

/***/ }),

/***/ 1141:
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__(0);
var router_1 = __webpack_require__(156);
var login_component_1 = __webpack_require__(1108);
var routes = [
    {
        path: '',
        component: login_component_1.LoginComponent,
        data: {
            title: 'login'
        }
    }
];
var LoginRoutingModule = (function () {
    function LoginRoutingModule() {
    }
    return LoginRoutingModule;
}());
LoginRoutingModule = __decorate([
    core_1.NgModule({
        imports: [router_1.RouterModule.forChild(routes)],
        exports: [router_1.RouterModule]
    })
], LoginRoutingModule);
exports.LoginRoutingModule = LoginRoutingModule;
//# sourceMappingURL=/home/admin1/Music/recenceptViewer.com UI/src/login-routing.module.js.map

/***/ }),

/***/ 1144:
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__(0);
var http_1 = __webpack_require__(103);
__webpack_require__(241);
__webpack_require__(824);
var repindexService = (function () {
    function repindexService(http) {
        this.http = http;
        this.url = "api/books";
    }
    repindexService.prototype.authenticationUser = function (userobject) {
    };
    return repindexService;
}());
repindexService = __decorate([
    core_1.Injectable(),
    __metadata("design:paramtypes", [typeof (_a = typeof http_1.Http !== "undefined" && http_1.Http) === "function" && _a || Object])
], repindexService);
exports.repindexService = repindexService;
var _a;
//# sourceMappingURL=/home/admin1/Music/recenceptViewer.com UI/src/repindex.service.js.map

/***/ }),

/***/ 1206:
/***/ (function(module, exports) {

module.exports = "     <div class=\"app flex-row align-items-center\" *ngIf=\"popupscreen\">\n      <div class=\"container\">\n    <div class=\"row justify-content-center\">\n     <div class=\"card col-5\" >\n        <div class=\"card-header\">\n          <strong>Set New Password</strong>\n        </div>\n        <div class=\"card-block\">\n          <div class=\"alert alert-danger\" *ngIf=passwordsame>\n           Password and confirm password must be same and cannot be empty\n          </div>\n           <div class=\"alert alert-success\" *ngIf=updatepasswordmg>\n            Password updated succssfully please login and verify\n          </div>\n          <form action=\"\" method=\"post\">\n            <div class=\"form-group\">\n              <label for=\"nf-email\">New Password</label>\n              <input type=\"password\" id=\"newpass\" name=\"newpass\" class=\"form-control\" placeholder=\"New password\" #newpass>\n            \n            </div>\n            <div class=\"form-group\">\n              <label for=\"nf-password\">Confirm Password</label>\n           <input type=\"password\" id=\"confirmpass\" name=\"confirmpass\" class=\"form-control\" placeholder=\"Confirm password\" #confirmpass>\n         \n            </div>\n          </form>\n        </div>\n      \n      <div class=\"modal-footer\">\n        \n         <a href=\"https://mattersmith1.embeddedexperience.com/review\">\n          <button class=\"btn btn-primary\">Login</button>\n          </a>\n        <button type=\"reset\" value=\"Reset\" class=\"btn btn-secondary\">Reset</button>\n        <button type=\"button\" class=\"btn btn-primary\" (click)=\"setPassword(newpass.value,confirmpass.value)\" >Submit</button>\n      </div>\n      </div>\n     </div>\n      </div>\n      </div>\n<div class=\"app flex-row align-items-center\" (ngSubmit)=\"authenticate(username.value,password.value)\" *ngIf=\"!popupscreen\">\n  <div class=\"container\">\n    <div class=\"row justify-content-center\">\n      <div class=\"col-md-5\">\n        <div class=\"card-group mb-0\">\n          <div class=\"card p-2\">\n            <div class=\"card-block\">\n              <img class=\"login-image container  d-flex justify-content-center\" src=\"assets/img/LOG.png\" style=\"height: 54px;margin-top: 0px; width: 70%;\">\n              <div class=\"alert alert-danger\" *ngIf=\"loginfail\">\n                Please check your username and password\n                </div>\n              <p class=\"text-muted\">Sign In to your Mattersmith Review account\n\n</p>\n              <div class=\"input-group mb-1\">\n                <span class=\"input-group-addon\"><i class=\"icon-user\"></i></span>\n                <input type=\"text\" (keyup)=\"validateUsername($event)\" class=\"form-control\" placeholder=\"Username\" #username>\n\n              \n              </div>\n                <div *ngIf=\"usernameStatusError\" class=\"alert alert-danger\">\n                Username is required\n                </div>\n              <div class=\"input-group mb-2\">\n                <span class=\"input-group-addon\"><i class=\"icon-lock\"></i></span>\n                <input type=\"password\" (keyup)=\"validatePassword($event)\" class=\"form-control\" placeholder=\"Password\" #password>\n                 \n              </div>\n               <div *ngIf=\"passwordStatusError\"class=\"alert alert-danger\">\n                Password is required\n                </div>\n              <div class=\"row\">\n                <div class=\"col-6\">\n                  <button style=\"background-color:#00B4AF\t;color:black;font-weight: bold;\" type=\"submit\" (click)=\"authenticate(username.value,password.value)\" [disabled]=\"!validationStatus\"  class=\"btn btn-primary px-2\">Login</button>\n                </div>\n                <div class=\"col-6 text-right\">\n                  <button style=\"color:#808080\t;\" type=\"button\" (click)=\"forgotPopUp()\"class=\"btn btn-link px-0\">Forgot password?</button>\n                </div>\n              </div>\n            </div>\n             <!-- <form action=\"https://www.sandbox.paypal.com/cgi-bin/webscr\" method=\"post\" target=\"_top\">\n    <input type=\"hidden\" name=\"cmd\" value=\"_s-xclick\">\n    <input type=\"hidden\" name=\"hosted_button_id\" value=\"VKDLCU6ZG8Q6J\">\n    <table>\n        <tr>\n            <td>\n                <input type=\"hidden\" name=\"on0\" value=\"\">\n            </td>\n        </tr>\n        <tr>\n            <td>\n                <select name=\"os0\">\n                    <option value=\"Repindex weekly Membership\">Repindex weekly Membership : $10.00 USD - weekly</option>\n                    <option value=\"Repindex monthly Membership\">Repindex monthly Membership : $20.00 USD - monthly</option>\n                    <option value=\"Repindex yearly Membership\">Repindex yearly Membership : $30.00 USD - yearly</option>\n                </select>\n            </td>\n        </tr>\n    </table>\n    <input type=\"hidden\" name=\"currency_code\" value=\"USD\">\n    <input type=\"image\" src=\"https://www.sandbox.paypal.com/en_US/i/btn/btn_subscribeCC_LG.gif\" border=\"0\" name=\"submit\" alt=\"PayPal - The safer, easier way to pay online!\">\n    <img alt=\"\" border=\"0\" src=\"https://www.sandbox.paypal.com/en_US/i/scr/pixel.gif\" width=\"1\" height=\"1\">\n</form>  -->\n       \n          </div>\n        \n        </div>\n      </div>\n    </div>\n  </div>\n</div>\n<button id=\"forgot\"  hidden type=\"button\" class=\"btn btn-secondary\" data-toggle=\"modal\"\n(click)=\"myModal.show()\">\n            Launch demo modal\n          </button>\n<div bsModal #myModal=\"bs-modal\" class=\"modal fade\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\" aria-hidden=\"true\">\n  <div class=\"modal-dialog\" role=\"document\">\n    <div class=\"modal-content\">\n      <div class=\"modal-header\" id=header>\n     \n          <h6 class=\"modal-title\" id=\"exampleModalLabel\" style=font-size:15px;>Forgot Password</h6>\n          \n        <button type=\"button\" class=\"close\" (click)=\"myModal.hide()\" aria-label=\"Close\">\n          <span aria-hidden=\"true\">&times;</span>\n        </button>\n      </div>\n       <div class=\"alert alert-danger\" *ngIf=errormsg>\n          Email with this user not associated\n          </div>\n          <div class=\"alert alert-success\" *ngIf=succssmsg>\n          Email Send Sucssfully Please check your Email\n          </div>\n      <div class=\"modal-body\" id=\"body\">\n <input type=\"text\" name=\"email\" id=\"email\" placeholder=\"Enter your email\" class=\"form-control\" #email>\n     \n    </div>\n      <div class=\"modal-footer\">\n        <button type=\"button\" class=\"btn btn-secondary\" (click)=\"myModal.hide()\">close</button>\n        <button type=\"button\" class=\"btn btn-primary\" (click)=\"checkEmail(email.value)\" >Submit</button>\n      </div>\n    </div><!-- /.modal-content -->\n  </div><!-- /.modal-dialog -->\n</div>\n<footer class=\"app-footer\" style=\"font-size:12px;margin-left:0px \">\n  <a href=\"http://coreui.io\"></a>  © Mattersmith Limited 2018.\n  <span class=\"float-right\">Powered By Repindex © <b style=\"color:orange\">Repindex</b> Limited 2018</span>\n</footer>\n\n\n"

/***/ }),

/***/ 749:
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__(0);
var ng2_charts_1 = __webpack_require__(410);
var dropdown_1 = __webpack_require__(412);
var common_1 = __webpack_require__(54);
var login_component_1 = __webpack_require__(1108);
var login_routing_module_1 = __webpack_require__(1141);
var repindex_service_1 = __webpack_require__(1144);
var login_service_1 = __webpack_require__(1109);
var http_1 = __webpack_require__(103);
var modal_1 = __webpack_require__(413);
var LoginModule = (function () {
    function LoginModule() {
    }
    return LoginModule;
}());
LoginModule = __decorate([
    core_1.NgModule({
        imports: [
            login_routing_module_1.LoginRoutingModule,
            ng2_charts_1.ChartsModule,
            modal_1.ModalModule.forRoot(),
            dropdown_1.DropdownModule, common_1.CommonModule, http_1.HttpModule
        ],
        declarations: [login_component_1.LoginComponent],
        providers: [
            repindex_service_1.repindexService, login_service_1.LoginService
        ],
    })
], LoginModule);
exports.LoginModule = LoginModule;
//# sourceMappingURL=/home/admin1/Music/recenceptViewer.com UI/src/login.module.js.map

/***/ }),

/***/ 793:
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var root_1 = __webpack_require__(53);
/* tslint:enable:max-line-length */
/**
 * Converts an Observable sequence to a ES2015 compliant promise.
 *
 * @example
 * // Using normal ES2015
 * let source = Rx.Observable
 *   .just(42)
 *   .toPromise();
 *
 * source.then((value) => console.log('Value: %s', value));
 * // => Value: 42
 *
 * // Rejected Promise
 * // Using normal ES2015
 * let source = Rx.Observable
 *   .throw(new Error('woops'))
 *   .toPromise();
 *
 * source
 *   .then((value) => console.log('Value: %s', value))
 *   .catch((err) => console.log('Error: %s', err));
 * // => Error: Error: woops
 *
 * // Setting via the config
 * Rx.config.Promise = RSVP.Promise;
 *
 * let source = Rx.Observable
 *   .of(42)
 *   .toPromise();
 *
 * source.then((value) => console.log('Value: %s', value));
 * // => Value: 42
 *
 * // Setting via the method
 * let source = Rx.Observable
 *   .just(42)
 *   .toPromise(RSVP.Promise);
 *
 * source.then((value) => console.log('Value: %s', value));
 * // => Value: 42
 *
 * @param PromiseCtor promise The constructor of the promise. If not provided,
 * it will look for a constructor first in Rx.config.Promise then fall back to
 * the native Promise constructor if available.
 * @return {Promise<T>} An ES2015 compatible promise with the last value from
 * the observable sequence.
 * @method toPromise
 * @owner Observable
 */
function toPromise(PromiseCtor) {
    var _this = this;
    if (!PromiseCtor) {
        if (root_1.root.Rx && root_1.root.Rx.config && root_1.root.Rx.config.Promise) {
            PromiseCtor = root_1.root.Rx.config.Promise;
        }
        else if (root_1.root.Promise) {
            PromiseCtor = root_1.root.Promise;
        }
    }
    if (!PromiseCtor) {
        throw new Error('no Promise impl found');
    }
    return new PromiseCtor(function (resolve, reject) {
        var value;
        _this.subscribe(function (x) { return value = x; }, function (err) { return reject(err); }, function () { return resolve(value); });
    });
}
exports.toPromise = toPromise;
//# sourceMappingURL=toPromise.js.map

/***/ }),

/***/ 824:
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var Observable_1 = __webpack_require__(7);
var toPromise_1 = __webpack_require__(793);
Observable_1.Observable.prototype.toPromise = toPromise_1.toPromise;
//# sourceMappingURL=toPromise.js.map

/***/ })

});
//# sourceMappingURL=8.chunk.js.map