webpackJsonp([13,16],{

/***/ 243:
/***/ (function(module, exports, __webpack_require__) {

"use strict";
// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `angular-cli.json`.

Object.defineProperty(exports, "__esModule", { value: true });
exports.environment = {
    production: false,
    baseUrl: "https://mattersmith1.embeddedexperience.com/repindexapi/"
};
//# sourceMappingURL=/home/admin1/Music/recenceptViewer.com UI/src/environment.js.map

/***/ }),

/***/ 332:
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__(0);
var layouts_service_1 = __webpack_require__(539);
var router_1 = __webpack_require__(156);
var ng2_translate_1 = __webpack_require__(160);
var _ = __webpack_require__(426);
var FullLayoutComponent = (function () {
    function FullLayoutComponent(Translate, router, UserService, activeRoute) {
        this.Translate = Translate;
        this.router = router;
        this.UserService = UserService;
        this.activeRoute = activeRoute;
        this.browserTitle = 'New Concept List';
        this.disabled = false;
        this.status = { isopen: false };
        this.conceptlists = {};
        this.concept = [];
        this.textlistfolders = [];
        this.newFolderData = {};
        this.renameFolderData = {};
        this.deleteFolderData = {};
        this.newTextListData = {};
        this.conceptListListData = {};
        this.conceptDeleteData = {};
        this.conceptRenameData = {};
        this.definationData = {};
        this.postsubfolderData = {};
        this.termslist = [];
        this.finaltermlist = [];
        this.multipleconceptList = [];
        this.subfoldername = "ssss";
        this.termsarry = [];
        this.conceptarray = [];
        this.loading = false;
        this.loader = false;
        this.uploaded = [];
        this.conceptListFile = [];
        this.addstopListFile = [];
        this.router = router;
        Translate.setDefaultLang("en");
        var lang = localStorage.getItem("lang");
        Translate.use(lang);
        this.concept_status = localStorage.getItem("conceptStatus");
        console.log("conceptstatus", this.concept_status);
    }
    FullLayoutComponent.prototype.toggled = function (open) {
    };
    FullLayoutComponent.prototype.toggleDropdown = function ($event) {
        $event.preventDefault();
        $event.stopPropagation();
        this.status.isopen = !this.status.isopen;
    };
    FullLayoutComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.userName = localStorage.getItem("username");
        this.username = localStorage.getItem("usertype");
        this.project = localStorage.getItem("addproject");
        var pid = localStorage.getItem('projectId');
        this.projectName = localStorage.getItem('projectName');
        this.UserService.getmasterlist().subscribe(function (data) {
            _this.masterConcepts = data;
            console.log("master concept", data);
        });
        this.UserService.getKnowledgeNode().subscribe(function (data) {
            console.log(data);
            _this.Nodes = data.nodes;
        });
        if (localStorage.getItem("addproject") == "beforeproject") {
            this.hide();
        }
        this.UserService.getstoplist().subscribe(function (data) {
            _this.getstop = data;
            console.log("stoplist", _this.getstop);
        });
        this.UserService.getConceptFolder(pid).subscribe(function (data) {
            _this.conceptFolder = data;
            console.log(_this.conceptFolder);
        });
        this.UserService.getData(pid).subscribe(function (data) {
            _this.urls = data.urls;
            _this.concept = data.concept_lists;
            console.log("bhin==", data);
            _this.text_folders = data.text_folders;
            _this.text_list_data = data.text_list_data;
            _this.textlistfolders = data.text_list_folders;
        });
    };
    FullLayoutComponent.prototype.renameTextListFolder = function (event) {
        document.getElementById("renameFolder").click();
        var i = document.getElementById("hiddenId-of").value;
        var name = document.getElementById("textOfFolder").value;
        document.getElementById("folderRenameId").value = i;
        document.getElementById("folderRenameText").value = name;
    };
    FullLayoutComponent.prototype.deletemultiple = function () {
        var _this = this;
        document.getElementById("deletemultiple").click();
        var pid = localStorage.getItem("projectId");
        this.UserService.getConceptlist(pid).subscribe(function (data) {
            console.log("==response==", data);
            _this.multipleconceptList = data;
        });
    };
    FullLayoutComponent.prototype.getmultipleconceptids = function (ids) {
        console.log(this.conceptarray);
        console.log(this.conceptarray.toString());
        this.conceptListListData.concept_id = this.conceptarray.toString();
        this.UserService.deleteMultipleConceptList(this.conceptListListData).subscribe(function (data) {
            window.location.reload();
        });
    };
    FullLayoutComponent.prototype.addNewSubTextList = function () {
        document.getElementById("addTextList").click();
        var i = document.getElementById("hiddenId-ofsubfolder").value;
        document.getElementById("id-hidden-for-new-text-list").value = i;
    };
    FullLayoutComponent.prototype.selectconceptvalue = function (color) {
        console.log("color", color);
        if (this.conceptarray.indexOf(color) === -1) {
            this.conceptarray.push(color);
        }
        else {
            var index = this.conceptarray.indexOf(color);
            this.conceptarray.splice(index, 1);
        }
    };
    FullLayoutComponent.prototype.closem1 = function () {
        document.getElementById("m1").style.display = "none";
    };
    FullLayoutComponent.prototype.closem3 = function () {
        document.getElementById("m3").style.display = "none";
    };
    FullLayoutComponent.prototype.closem2 = function () {
        document.getElementById("m2").style.display = "none";
    };
    FullLayoutComponent.prototype.closeM5 = function () {
        document.getElementById("m5").style.display = "none";
    };
    FullLayoutComponent.prototype.closeMM9 = function () {
        document.getElementById("mm9").style.display = "none";
    };
    FullLayoutComponent.prototype.closem4 = function () {
        document.getElementById("m4").style.display = "none";
    };
    FullLayoutComponent.prototype.closeStopList = function () {
        document.getElementById("m10").style.display = "none";
    };
    FullLayoutComponent.prototype.closeStopListChild = function () {
        document.getElementById("m11").style.display = "none";
    };
    FullLayoutComponent.prototype.renameFolder = function () {
        var text = document.getElementById("folderRenameText").value;
        var id = document.getElementById("folderRenameId").value;
        if (text.length <= 12) {
            this.renameFolderData.folder_name = text;
            this.UserService.renameFolder(id, this.renameFolderData).subscribe(function (data) {
                setTimeout(function () {
                    location.reload();
                }, 1000);
            });
        }
        else {
            alert("A folder name must be less than 12 characters");
        }
    };
    FullLayoutComponent.prototype.addNewTextList = function () {
        document.getElementById("addTextList").click();
        var i = document.getElementById("hiddenId-of").value;
        document.getElementById("id-hidden-for-new-text-list").value = i;
    };
    FullLayoutComponent.prototype.openlimit = function () {
        document.getElementById("limit").click();
    };
    FullLayoutComponent.prototype.setlimit = function (limit) {
        console.log(limit);
        localStorage.removeItem("setlimit");
        localStorage.setItem("setlimit", limit);
        window.location.reload();
    };
    FullLayoutComponent.prototype.userComment = function (conceptId) {
        document.getElementById("defination").click();
        var id = document.getElementById("conceptid").value;
        this.UserService.getDefination(id).subscribe(function (data) {
            console.log("response==>", data);
            console.log("response===>", data[0].fields.defination);
            document.getElementById("def").value = data[0].fields.defination;
        });
    };
    FullLayoutComponent.prototype.saveDef = function (def) {
        console.log(def);
        var id = document.getElementById("conceptid").value;
        console.log(id);
        this.definationData.id = id;
        this.definationData.def = def;
        console.log("=========", this.definationData);
        this.UserService.addDefination(this.definationData).subscribe(function (data) {
            console.log("==response==", data);
            window.location.reload();
        });
    };
    FullLayoutComponent.prototype.fileChangeEvent = function (event) {
        var fileList = event.target.files;
        var data = [];
        for (var i = 0; i < fileList.length; i++) {
            var file = fileList[i];
            data[i] = (fileList[i]);
            if (data[i].size > 1024000) {
                alert("Document size should be less than 1MB. Please compress Document by Clicking on 'Compress Files' link");
                window.location.reload();
            }
            var lengthOftheFile = data[i].name.length;
            var reducewiththree = data[i].name.length - 3;
            var pdfExtension = data[i].name.substring(reducewiththree, lengthOftheFile);
            if (pdfExtension == "pdf") {
                alert("Please convert pdf file to docx file by Clicking on the 'PDF to DOCX' link");
                window.location.reload();
            }
        }
        this.uploaded = data;
    };
    FullLayoutComponent.prototype.fileChangeEvent2 = function (event) {
        var fileList = event.target.files;
        var file = fileList[0];
        this.conceptListFile = fileList[0];
    };
    FullLayoutComponent.prototype.addTextList = function () {
        this.loaderFlag = 'loader';
        var x = document.getElementById("newTextListFile").value;
        var id = document.getElementById("id-hidden-for-new-text-list").value;
        var minLegth = (this.uploaded).length;
        if (minLegth <= 10) {
            var access_token = localStorage.getItem("access_token");
            this.loading = true;
            this.UserService.addNewTextList(localStorage.getItem('userId'), localStorage.getItem('projectId'), id, access_token, this.uploaded).subscribe(function (data) {
                setTimeout(function () {
                    location.reload();
                }, 1000);
            });
        }
        else {
            alert("You may upload a maximum of 10 documents at a time");
            setTimeout(function () {
                location.reload();
            }, 100);
        }
    };
    FullLayoutComponent.prototype.newConceptListImport = function () {
        this.conceptlistfolderId = document.getElementById("hiddenId-of-concept-list-folder-id").value;
        document.getElementById("addConceptList").click();
    };
    FullLayoutComponent.prototype.newConceptStopListImport = function () {
        document.getElementById("addConceptStopList").click();
    };
    FullLayoutComponent.prototype.addConceptList = function () {
        this.loader = true;
        this.loaderFlag = 'loader';
        this.UserService.addNewConceptList(this.conceptlistfolderId, this.conceptListFile).subscribe(function (data) {
            console.log(data);
            if (data.success == 'alert') {
                alert("A concept list may contain a maximum of 200 concepts. Any list with more than 200 will be cut short.");
            }
            if (data.success == "false") {
                alert("A concept list with the same name as an existing one cannot be uploaded");
            }
            setTimeout(function () {
                location.reload();
            }, 100);
        });
    };
    FullLayoutComponent.prototype.fileChangeEventStopList = function (event) {
        var fileList = event.target.files;
        var file = fileList[0];
        this.addstopListFile = fileList[0];
        console.log("===============event  event===", this.addstopListFile);
        console.log("===============event  event===");
    };
    FullLayoutComponent.prototype.addStopList = function () {
        console.log("===============addstopListFile===", this.addstopListFile);
        this.loader = true;
        this.UserService.addNewStopList(this.addstopListFile).subscribe(function (data) {
            console.log(data);
            setTimeout(function () {
                location.reload();
            }, 100);
        });
    };
    FullLayoutComponent.prototype.hide = function () {
        document.getElementById("toglerbtn").click();
        document.getElementById("toglerbtn").style.display = "none";
    };
    FullLayoutComponent.prototype.showConceptListMenu = function (event) {
        document.getElementById("m4").style.display = "block";
        var target = event.target || event.srcElement || event.currentTarget;
        var name = target.id.substring(0, target.id.indexOf('@'));
        var id = target.id.substring(target.id.indexOf("@") + 1);
        document.getElementById("hiddenId-of-concept-list").value = id;
        document.getElementById("textOfConceptList").value = name;
        return false;
    };
    FullLayoutComponent.prototype.logout = function () {
        localStorage.clear();
    };
    FullLayoutComponent.prototype.ConceptListMenuChild = function (event, child) {
        this.childData = child;
        document.getElementById("mm9").style.display = "block";
        var target = event.target || event.srcElement || event.currentTarget;
        var name = target.id.substring(0, target.id.indexOf('@'));
        var id = target.id.substring(target.id.indexOf("@") + 1);
        document.getElementById("hidenTermsOfUmbrellaChild").value = id;
        document.getElementById("hidenUmbrellaChild").value = name;
        return false;
    };
    FullLayoutComponent.prototype.ConceptStopListMenu = function (event) {
        //this.stopList = stopList
        this.browserTitle = "New Stop list";
        document.getElementById("m10").style.display = "block";
        return false;
    };
    FullLayoutComponent.prototype.ConceptStopListMenuChild = function (event) {
        document.getElementById("m11").style.display = "block";
        var target = event.target || event.srcElement || event.currentTarget;
        var name = target.id.substring(0, target.id.indexOf('@'));
        var id = target.id.substring(target.id.indexOf("@") + 1);
        document.getElementById("hiddenId-of-stop-list").value = id;
        document.getElementById("textOfStopList").value = name;
        return false;
    };
    FullLayoutComponent.prototype.demo1 = function () {
        document.getElementById("m1").style.display = "block";
        return false;
    };
    FullLayoutComponent.prototype.demo2 = function () {
        document.getElementById("m2").style.display = "block";
        return false;
    };
    FullLayoutComponent.prototype.demo3 = function (event) {
        document.getElementById("m3").style.display = "block";
        var target = event.target || event.srcElement || event.currentTarget;
        var id = target.id.substring(0, target.id.indexOf('@'));
        var name = target.id.substring(target.id.indexOf("@") + 1);
        document.getElementById("hiddenId-of").value = id;
        document.getElementById("textOfFolder").value = name;
        return false;
    };
    FullLayoutComponent.prototype.addNewFolderToTextList = function () {
        document.getElementById("uniq").click();
    };
    FullLayoutComponent.prototype.createNewFolder = function () {
        this.newFolderData.folder_name = document.getElementById("text-list-new-folder-name").value;
        var minLength = document.getElementById("text-list-new-folder-name").value.length;
        if (minLength < 15) {
            this.UserService.addFolder(this.newFolderData, localStorage.getItem("userId"), localStorage.getItem("projectId")).subscribe(function (data) {
                setTimeout(function () {
                    location.reload();
                }, 1000);
            });
        }
        else {
            alert("Folder Name should be less than 15 char");
        }
    };
    FullLayoutComponent.prototype.deleteTextListFolder = function () {
        document.getElementById("deleteFolder").click();
        var i = document.getElementById("hiddenId-of").value;
        document.getElementById("folderDeleteId").value = i;
    };
    FullLayoutComponent.prototype.deleteFolder = function () {
        var id = document.getElementById("folderDeleteId").value;
        this.UserService.deleteFolder(id, this.deleteFolderData).subscribe(function (data) {
            setTimeout(function () {
                location.reload();
            }, 1000);
        });
    };
    FullLayoutComponent.prototype.showConceptListMenuUmbrella = function (event) {
        var target = event.target || event.srcElement || event.currentTarget;
        console.log("====target===", target);
        var umbrella = target.id.substring(0, target.id.indexOf('@'));
        var termsraw = target.id.substring(target.id.indexOf("@") + 1);
        var termlen = termsraw.length;
        var termdiff = termlen - 5;
        var terms = termsraw.substring(0, termdiff);
        var conceptid = target.id.substring(target.id.indexOf("@"));
        var splitstr = conceptid.split("@");
        var updatedterms = splitstr[splitstr.length - 2];
        var conceptidvar = splitstr[splitstr.length - 1];
        document.getElementById("hidenTermsOfUmbrella").value = updatedterms;
        document.getElementById("hidenUmbrella").value = umbrella;
        document.getElementById("conceptid").value = conceptidvar;
        document.getElementById("m5").style.display = "block";
        return false;
    };
    FullLayoutComponent.prototype.insertIntosearch = function () {
        var terms = document.getElementById("hidenTermsOfUmbrella").value;
        var umbrella = document.getElementById("hidenUmbrella").value;
        document.getElementById("lst_toi").value = terms;
        document.getElementById("lst_toi1").value = umbrella;
    };
    FullLayoutComponent.prototype.selectTerms = function () {
        this.termslist = [];
        document.getElementById("terms").click();
        //termsvalue
        var container = document.getElementById("termsvalue");
        var terms = document.getElementById("hidenTermsOfUmbrella").value;
        var temp = new Array();
        temp = terms.split(",");
        this.termslist.push(temp);
        this.finaltermlist = this.termslist[0];
        console.log("this.finaltermlist", this.finaltermlist);
    };
    FullLayoutComponent.prototype.selecttermsvalue = function (color) {
        console.log("color", color);
        if (this.termsarry.indexOf(color) === -1) {
            this.termsarry.push(color);
        }
        else {
            var index = this.termsarry.indexOf(color);
            this.termsarry.splice(index, 1);
        }
        console.log("selected terms=======>", this.termsarry);
    };
    FullLayoutComponent.prototype.insertTerms = function () {
        console.log("insertTerms=======>", this.termsarry);
        document.getElementById("lst_toi").value = this.termsarry;
        document.getElementById("lst_toi1").value = this.termsarry;
    };
    FullLayoutComponent.prototype.insertIntosearchChild = function () {
        document.getElementById("lst_toi").value = this.childData;
        document.getElementById("lst_toi1").value = this.childData;
    };
    FullLayoutComponent.prototype.ConceptListMenuStopList = function () {
    };
    FullLayoutComponent.prototype.insertintoand = function () {
        var terms = document.getElementById("hidenTermsOfUmbrella").value;
        document.getElementById("and").value = this.childData;
    };
    FullLayoutComponent.prototype.renameConceptList = function () {
        document.getElementById("renameConceptList").click();
        var i = document.getElementById("hiddenId-of-concept-list").value;
        var name = document.getElementById("textOfConceptList").value;
        document.getElementById("pop-up-concpt-id").value = i;
        document.getElementById("pop-up-concept-name").value = name;
        // conceptRenameData
    };
    FullLayoutComponent.prototype.renameeConceptList = function () {
        var id = document.getElementById("pop-up-concpt-id").value;
        var name = document.getElementById("pop-up-concept-name").value;
        this.conceptRenameData.concept_id = id;
        this.conceptRenameData.rename_list = name;
        this.UserService.renameConceptList(this.conceptRenameData).subscribe(function (data) {
            setTimeout(function () {
                location.reload();
            }, 1000);
        });
    };
    FullLayoutComponent.prototype.deleteConceptList = function () {
        document.getElementById("deleteConceptList").click();
        var i = document.getElementById("hiddenId-of-concept-list").value;
        var name = document.getElementById("textOfConceptList").value;
        document.getElementById("delete-concept-list-hidden-pop").value = i;
    };
    FullLayoutComponent.prototype.deleteeConceptList = function () {
        this.conceptDeleteData.concept_id = document.getElementById("delete-concept-list-hidden-pop").value;
        this.UserService.deleteConceptList(this.conceptDeleteData).subscribe(function (data) {
            setTimeout(function () {
                location.reload();
            }, 1000);
        });
    };
    FullLayoutComponent.prototype.renameStopList = function () {
        document.getElementById("renameStopList").click();
        var id = document.getElementById("hiddenId-of-stop-list").value;
        var name = document.getElementById("textOfStopList").value;
        document.getElementById("pop-up-stop-id").value = id;
        document.getElementById("pop-up-stop-name").value = name;
    };
    FullLayoutComponent.prototype.renameGivenStopList = function () {
        var id = document.getElementById("pop-up-stop-id").value;
        var name = document.getElementById("pop-up-stop-name").value;
        var data = {
            'id': id,
            'name': name,
        };
        this.UserService.renameStopList(data).subscribe(function (data) {
            setTimeout(function () {
                location.reload();
            }, 1000);
        });
    };
    FullLayoutComponent.prototype.deleteStopList = function (deletestopListId) {
        this.deletStopListId = deletestopListId;
        document.getElementById("deleteStopList").click();
    };
    FullLayoutComponent.prototype.deletedStopList = function () {
        this.UserService.deleteStopList(this.deletStopListId).subscribe(function (data) {
            setTimeout(function () {
                location.reload();
            }, 1000);
        });
    };
    FullLayoutComponent.prototype.allClose = function () {
        /*
         document.getElementById("m1").style.display = "none";
         document.getElementById("m3").style.display = "none";
          document.getElementById("m2").style.display = "none";
           document.getElementById("m4").style.display = "none";
            document.getElementById("m5").style.display = "none";
          */
        console.log("in all close");
        $("#m1").css("display", "none");
        $("#m2").css("display", "none");
        $("#p1").css("display", "none");
        $("#m3").css("display", "none");
        $("#m4").css("display", "none");
        $("#m5").css("display", "none");
        $("#m11").css("display", "none");
        $("#m10").css("display", "none");
        $("#mm5").css("display", "none");
        $("#mm3").css("display", "none");
        $("#masterChileId").css("display", "none");
        $("#masterlist").css("display", "none");
        $("#mm9").css("display", "none");
        $("#url_menu").css("display", "none");
        $("#gotoknowledge").css("display", "none");
    };
    FullLayoutComponent.prototype.newQueryPgae = function () {
        var url = localStorage.getItem("url");
        console.log("navigate to project", url);
        window.location.href = url;
        setTimeout(function () { window.location.reload(); }, 15);
    };
    FullLayoutComponent.prototype.TextListMenuChild = function (event, child) {
        document.getElementById("p1").style.display = "block";
        var target = event.target || event.srcElement || event.currentTarget;
        console.log(target, "------------");
        var name = target.id.substring(0, target.id.indexOf('@'));
        var id = target.id.substring(target.id.indexOf("@") + 1);
        console.log("name", name);
        console.log("id", id);
        document.getElementById("hiddenId-of-text-list").value = id;
        document.getElementById("textListId").value = name;
        return false;
    };
    FullLayoutComponent.prototype.deleteTextListData = function () {
        document.getElementById("deleteTextList").click();
        var i = document.getElementById("hiddenId-of-text-list").value;
        var name = document.getElementById("textListId").value;
        document.getElementById("delete-text-list-hidden-pop").value = i;
    };
    FullLayoutComponent.prototype.deleteeTextList = function () {
        var dataId = document.getElementById("hiddenId-of-text-list").value;
        console.log("id===>", dataId);
        var data = { 'id': dataId };
        this.UserService.deleteTextList(data).subscribe(function (data) {
            setTimeout(function () {
                location.reload();
            }, 1000);
        });
    };
    FullLayoutComponent.prototype.closeTxtList = function () {
        document.getElementById("p1").style.display = "none";
    };
    FullLayoutComponent.prototype.addSubfolder = function () {
        document.getElementById("subfolder").click();
        var i = document.getElementById("hiddenId-of").value;
        document.getElementById("hiddenfolderId").value = i;
    };
    FullLayoutComponent.prototype.addsub = function () {
        var id = (document.getElementById("hiddenfolderId").value);
        console.log(id);
        var name = (document.getElementById("sub").value);
        this.postsubfolderData.folder_name = name;
        this.postsubfolderData.folder_id = id;
        console.log(this.postsubfolderData);
        if (name.length <= 12) {
            this.UserService.addsubFolder(localStorage.getItem("userId"), localStorage.getItem("projectId"), this.postsubfolderData).subscribe(function (data) {
                setTimeout(function () {
                    location.reload();
                }, 1000);
            });
        }
        else {
            alert("A folder name must be less than 12 characters");
        }
    };
    FullLayoutComponent.prototype.addNewFolderToConceptList = function () {
        document.getElementById("clist").click();
    };
    FullLayoutComponent.prototype.demos3 = function (event) {
        document.getElementById("mm3").style.display = "block";
        var target = event.target || event.srcElement || event.currentTarget;
        var id = target.id.substring(0, target.id.indexOf('@'));
        var name = target.id.substring(target.id.indexOf("@") + 1);
        document.getElementById("hiddenId-ofsubfolder").value = id;
        document.getElementById("textOfsubFolder").value = name;
        return false;
    };
    FullLayoutComponent.prototype.demo4 = function (event) {
        document.getElementById("mm5").style.display = "block";
        var target = event.target || event.srcElement || event.currentTarget;
        var id = target.id.substring(0, target.id.indexOf('@'));
        var name = target.id.substring(target.id.indexOf("@") + 1);
        document.getElementById("hiddenId-of-concept-list-folder-id").value = id;
        document.getElementById("textOfConceptListFolder").value = name;
        return false;
    };
    FullLayoutComponent.prototype.createNewFolderConceptList = function () {
        var name = document.getElementById("concept-list-new-folder-name").value;
        var data = { 'name': name };
        this.UserService.addConceptFolder(data, localStorage.getItem("userId"), localStorage.getItem("projectId")).subscribe(function (data) {
            setTimeout(function () {
                location.reload();
            }, 1000);
        });
    };
    FullLayoutComponent.prototype.renameConceptListFolder = function (event) {
        document.getElementById("renameConceptFolder").click();
        var i = document.getElementById("hiddenId-of-concept-list-folder-id").value;
        var name = document.getElementById("textOfConceptListFolder").value;
        document.getElementById("folderRenameConceptId").value = i;
        document.getElementById("folderRenameConceptText").value = name;
    };
    FullLayoutComponent.prototype.deleteConceptListFolder = function () {
        document.getElementById("deleteConceptFolder").click();
        this.deleteConceptFolderId = document.getElementById("hiddenId-of-concept-list-folder-id").value;
        document.getElementById("textOfConceptListFolder").value = this.deleteConceptFolderId;
    };
    FullLayoutComponent.prototype.renameFolderConcept = function () {
        var text = document.getElementById("folderRenameConceptText").value;
        var id = document.getElementById("folderRenameConceptId").value;
        this.UserService.renameFolderConcept(id, { 'name': text }).subscribe(function (data) {
            setTimeout(function () {
                location.reload();
            }, 1000);
        });
    };
    FullLayoutComponent.prototype.deleteConceptFolder = function () {
        this.UserService.deleteConceptFolder({ 'id': this.deleteConceptFolderId }).subscribe(function (data) {
            setTimeout(function () {
                location.reload();
            }, 1000);
        });
    };
    FullLayoutComponent.prototype.deleteSubTextListFolder = function () {
        document.getElementById("deleteFolder").click();
        var i = document.getElementById("hiddenId-ofsubfolder").value;
        document.getElementById("folderDeleteId").value = i;
    };
    FullLayoutComponent.prototype.renameSubTextListFolder = function (event) {
        document.getElementById("renameFolder").click();
        var i = document.getElementById("hiddenId-ofsubfolder").value;
        var name = document.getElementById("textOfsubFolder").value;
        document.getElementById("folderRenameId").value = i;
        document.getElementById("folderRenameText").value = name;
    };
    FullLayoutComponent.prototype.importUrl = function () {
        document.getElementById("addurl").click();
        var i = document.getElementById("hiddenId-of").value;
        document.getElementById("hiddenurlId").value = i;
    };
    FullLayoutComponent.prototype.addUrl = function () {
        var data = {};
        data['name'] = (document.getElementById("urlname").value);
        data['url'] = (document.getElementById("url").value);
        data['folder_id'] = document.getElementById("hiddenId-of").value;
        console.log("addurl===>", data);
        this.UserService.addUrl(localStorage.getItem("projectId"), data).subscribe(function (data) {
            location.reload();
        });
    };
    FullLayoutComponent.prototype.TextListUrl = function (event, data) {
        console.log(data);
        document.getElementById("url_menu").style.display = "block";
        var target = event.target || event.srcElement || event.currentTarget;
        var name = target.id.substring(0, target.id.indexOf('@'));
        document.getElementById("hiddenId-of-text-list-url").value = data;
        document.getElementById("textListurlId").value = name;
        return false;
    };
    FullLayoutComponent.prototype.deleteTextListUrl = function () {
        document.getElementById("delete-text-listurl").click();
        var i = document.getElementById("hiddenId-of-text-list-url").value;
        var name = document.getElementById("textListurlId").value;
        document.getElementById("delete-text-list-url").value = i;
    };
    FullLayoutComponent.prototype.deleteUrl = function () {
        var dataId = document.getElementById("delete-text-list-url").value;
        var data = { 'id': dataId };
        this.UserService.deleteURL(data).subscribe(function (data) {
            location.reload();
        });
    };
    FullLayoutComponent.prototype.closeurl = function () {
        document.getElementById("url_menu").style.display = "none";
    };
    FullLayoutComponent.prototype.deleteeMasterList = function () {
        var dataId = document.getElementById("delete-master-list-hidden-pop").value;
        this.UserService.deleteMasterList(dataId).subscribe(function (data) {
            location.reload();
        });
    };
    FullLayoutComponent.prototype.renameGivenMasterList = function () {
        var id = document.getElementById("pop-up-master-id").value;
        var name = document.getElementById("pop-up-master-name").value;
        var data = {
            'id': id,
            'name': name,
        };
        this.UserService.renameMasterList(data).subscribe(function (data) {
            setTimeout(function () {
                location.reload();
            }, 1000);
        });
    };
    FullLayoutComponent.prototype.fileChangeEventMasterList = function (event) {
        console.log(event.target.files);
        var fileList = event.target.files;
        var file = fileList[0];
        this.masterList = fileList[0];
    };
    FullLayoutComponent.prototype.addMasterList = function () {
        console.log("===============addMasterListFile===", this.masterList);
        this.loaderFlag = 'loader';
        this.UserService.addNewMasterList(this.masterList).subscribe(function (data) {
            console.log(data);
            setTimeout(function () {
                location.reload();
            }, 100);
        });
    };
    FullLayoutComponent.prototype.showMasterList = function (event) {
        //this.stopList = stopList
        this.browserTitle = "New Master Concept List";
        document.getElementById("masterlist").style.display = "block";
        return false;
    };
    FullLayoutComponent.prototype.newMasterListImport = function () {
        document.getElementById("addMasterList").click();
    };
    FullLayoutComponent.prototype.renameMasterList = function () {
        document.getElementById("myModalMaster").click();
        var id = document.getElementById("hiddenId-of-master-list").value;
        var name = '';
        for (var i in this.masterConcepts) {
            console.log(this.masterConcepts[i]['name']);
            if (Number(this.masterConcepts[i]['id']) == Number(id)) {
                name = this.masterConcepts[i]['name'];
                break;
            }
        }
        document.getElementById("pop-up-master-id").value = id;
        document.getElementById("pop-up-master-name").value = name;
    };
    FullLayoutComponent.prototype.deleteMasterList = function (deleteMasterListId) {
        this.deleteMasterListId = deleteMasterListId;
        document.getElementById("deleteMasterList").click();
        var i = document.getElementById("hiddenId-of-master-list").value;
        var name = document.getElementById("textOfMasterList").value;
        document.getElementById("delete-master-list-hidden-pop").value = i;
    };
    FullLayoutComponent.prototype.closemasterChild = function () {
        document.getElementById("masterlist").style.display = "none";
    };
    FullLayoutComponent.prototype.MasterListMenuChild = function (event) {
        document.getElementById("masterChileId").style.display = "block";
        var target = event.target || event.srcElement || event.currentTarget;
        var name = target.id.substring(0, target.id.indexOf('@'));
        var id = target.id.substring(target.id.indexOf("@") + 1);
        document.getElementById("hiddenId-of-master-list").value = id;
        document.getElementById("textOfMasterList").value = name;
        return false;
    };
    FullLayoutComponent.prototype.gotoKnowledge = function () {
        var reviewConceptName = $("#hidenUmbrella").val();
        var nodefound = false;
        _.filter(this.Nodes, function (Node) {
            if (Node.class === "Playbook" && Node.props._oid) {
                console.log("props", Node.props.name.toUpperCase());
                if ((reviewConceptName.toUpperCase()).trim() == Node.props.name.toUpperCase()) {
                    nodefound = false;
                    window.open("https://mattersmith1.embeddedexperience.com/#playbook_openOid=" + Node.props._oid + "&filter_includeManual=" + Node.props._oid + "&select=" + Node.props._oid + "&v=1&path=" + Node.props.attachment);
                }
                else {
                    nodefound = true;
                }
                if (nodefound != true) {
                    this.ngOnInit();
                }
            }
        });
        if (nodefound && true) {
            document.getElementById("cancelModalConcept").click();
        }
    };
    return FullLayoutComponent;
}());
FullLayoutComponent = __decorate([
    core_1.Component({
        selector: 'app-dashboard',
        template: __webpack_require__(719),
        providers: [layouts_service_1.LayoutService],
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof ng2_translate_1.TranslateService !== "undefined" && ng2_translate_1.TranslateService) === "function" && _a || Object, typeof (_b = typeof router_1.Router !== "undefined" && router_1.Router) === "function" && _b || Object, typeof (_c = typeof layouts_service_1.LayoutService !== "undefined" && layouts_service_1.LayoutService) === "function" && _c || Object, typeof (_d = typeof router_1.ActivatedRoute !== "undefined" && router_1.ActivatedRoute) === "function" && _d || Object])
], FullLayoutComponent);
exports.FullLayoutComponent = FullLayoutComponent;
var _a, _b, _c, _d;
//# sourceMappingURL=/home/admin1/Music/recenceptViewer.com UI/src/full-layout.component.js.map

/***/ }),

/***/ 333:
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__(0);
var SimpleLayoutComponent = (function () {
    function SimpleLayoutComponent() {
    }
    SimpleLayoutComponent.prototype.ngOnInit = function () {
        this.username = localStorage.getItem("username");
    };
    SimpleLayoutComponent.prototype.logout = function () {
        localStorage.clear();
        window.location.reload();
    };
    return SimpleLayoutComponent;
}());
SimpleLayoutComponent = __decorate([
    core_1.Component({
        selector: 'app-dashboard',
        template: '<router-outlet><header class="app-header navbar"><img src="assets/img/LOG.png" width=170><p class="left"> <a *ngIf="username" class="dropdown-item" href="#" (click)="logout()"><i class="fa fa-lock"></i> LogOut</a></p></header></router-outlet>',
        styles: ["\n    .left{\n     margin-left:74% !important;\n     height:10px !important;\n\n    }\n    .dropdown-item{\n    background-color:#003746 !important;\n    margin-left: 75px;\n    margin-top: 10px;\n    color:white;\n    },\n  "],
    }),
    __metadata("design:paramtypes", [])
], SimpleLayoutComponent);
exports.SimpleLayoutComponent = SimpleLayoutComponent;
//# sourceMappingURL=/home/admin1/Music/recenceptViewer.com UI/src/simple-layout.component.js.map

/***/ }),

/***/ 393:
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./charts/charts.module": [
		744,
		3
	],
	"./components/components.module": [
		745,
		6
	],
	"./dashboard/dashboard.module": [
		746,
		2
	],
	"./editor/editor.module": [
		747,
		0
	],
	"./icons/icons.module": [
		748,
		9
	],
	"./login/login.module": [
		749,
		8
	],
	"./pages/pages.module": [
		750,
		7
	],
	"./project/project.module": [
		751,
		5
	],
	"./superdashboard/superdashboard.module": [
		752,
		1
	],
	"./user/user.module": [
		753,
		4
	],
	"./widgets/widgets.module": [
		754,
		10
	]
};
function webpackAsyncContext(req) {
	var ids = map[req];	if(!ids)
		return Promise.reject(new Error("Cannot find module '" + req + "'."));
	return __webpack_require__.e(ids[1]).then(function() {
		return __webpack_require__(ids[0]);
	});
};
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
module.exports = webpackAsyncContext;
webpackAsyncContext.id = 393;


/***/ }),

/***/ 394:
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__(0);
var platform_browser_dynamic_1 = __webpack_require__(506);
var app_module_1 = __webpack_require__(537);
var environment_1 = __webpack_require__(243);
if (environment_1.environment.production) {
    core_1.enableProdMode();
}
platform_browser_dynamic_1.platformBrowserDynamic().bootstrapModule(app_module_1.AppModule);
//# sourceMappingURL=/home/admin1/Music/recenceptViewer.com UI/src/main.js.map

/***/ }),

/***/ 536:
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__(0);
var AppComponent = (function () {
    function AppComponent() {
    }
    return AppComponent;
}());
AppComponent = __decorate([
    core_1.Component({
        // tslint:disable-next-line
        selector: 'body',
        template: '<router-outlet></router-outlet>'
    })
], AppComponent);
exports.AppComponent = AppComponent;
//# sourceMappingURL=/home/admin1/Music/recenceptViewer.com UI/src/app.component.js.map

/***/ }),

/***/ 537:
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var platform_browser_1 = __webpack_require__(106);
var core_1 = __webpack_require__(0);
var common_1 = __webpack_require__(54);
var app_component_1 = __webpack_require__(536);
var dropdown_1 = __webpack_require__(412);
var tabs_1 = __webpack_require__(427);
var nav_dropdown_directive_1 = __webpack_require__(542);
var ng2_translate_1 = __webpack_require__(160);
var ng2_charts_1 = __webpack_require__(410);
var sidebar_directive_1 = __webpack_require__(543);
var aside_directive_1 = __webpack_require__(540);
var breadcrumb_component_1 = __webpack_require__(541);
var http_1 = __webpack_require__(103);
// Routing Module
var app_routing_1 = __webpack_require__(538);
// Layouts
var full_layout_component_1 = __webpack_require__(332);
var simple_layout_component_1 = __webpack_require__(333);
var ng2_right_click_menu_1 = __webpack_require__(706);
//import { LoginComponent } from './login/login.component';
var modal_1 = __webpack_require__(413);
var ng2_translate_2 = __webpack_require__(160);
var http_2 = __webpack_require__(103);
function httpFactory(http) {
    return new ng2_translate_2.TranslateStaticLoader(http, '/review/assets/i18n', '.json');
}
exports.httpFactory = httpFactory;
var AppModule = (function () {
    function AppModule() {
    }
    return AppModule;
}());
AppModule = __decorate([
    core_1.NgModule({
        imports: [
            platform_browser_1.BrowserModule,
            app_routing_1.AppRoutingModule,
            dropdown_1.DropdownModule.forRoot(),
            modal_1.ModalModule.forRoot(),
            tabs_1.TabsModule.forRoot(),
            ng2_charts_1.ChartsModule, http_1.HttpModule, ng2_right_click_menu_1.ShContextMenuModule,
            ng2_translate_1.TranslateModule.forRoot({
                provide: ng2_translate_2.TranslateLoader,
                useFactory: httpFactory,
                deps: [http_2.Http]
            })
        ],
        declarations: [
            app_component_1.AppComponent,
            full_layout_component_1.FullLayoutComponent,
            simple_layout_component_1.SimpleLayoutComponent,
            nav_dropdown_directive_1.NAV_DROPDOWN_DIRECTIVES,
            breadcrumb_component_1.BreadcrumbsComponent,
            sidebar_directive_1.SIDEBAR_TOGGLE_DIRECTIVES,
            aside_directive_1.AsideToggleDirective,
        ],
        providers: [{
                provide: common_1.LocationStrategy,
                useClass: common_1.HashLocationStrategy,
            }],
        bootstrap: [app_component_1.AppComponent]
    })
], AppModule);
exports.AppModule = AppModule;
//# sourceMappingURL=/home/admin1/Music/recenceptViewer.com UI/src/app.module.js.map

/***/ }),

/***/ 538:
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__(0);
var router_1 = __webpack_require__(156);
// Layouts
var full_layout_component_1 = __webpack_require__(332);
var simple_layout_component_1 = __webpack_require__(333);
exports.routes = [
    {
        path: '',
        redirectTo: 'login',
        pathMatch: 'full',
    },
    {
        path: '',
        component: simple_layout_component_1.SimpleLayoutComponent,
        data: {
            title: 'Home'
        },
        children: [
            {
                path: 'login',
                loadChildren: './login/login.module#LoginModule'
            },
            {
                path: 'login/token/:any',
                loadChildren: './login/login.module#LoginModule'
            },
            {
                path: 'components',
                loadChildren: './components/components.module#ComponentsModule'
            },
            {
                path: 'icons',
                loadChildren: './icons/icons.module#IconsModule'
            },
            {
                path: 'widgets',
                loadChildren: './widgets/widgets.module#WidgetsModule'
            },
            {
                path: 'charts',
                loadChildren: './charts/charts.module#chartsModule'
            },
            {
                path: 'project',
                loadChildren: './project/project.module#projectModule'
            }
        ]
    },
    {
        path: '',
        component: full_layout_component_1.FullLayoutComponent,
        data: {
            title: 'Pages'
        },
        children: [
            {
                path: 'dashboard',
                loadChildren: './dashboard/dashboard.module#DashboardModule'
            },
            {
                path: 'superdashboard',
                loadChildren: './superdashboard/superdashboard.module#superDashboardModule'
            },
            {
                path: 'superdashboard',
                loadChildren: './superdashboard/superdashboard.module#superDashboardModule'
            },
            { path: 'editor',
                loadChildren: './editor/editor.module#editorModule'
            },
            {
                path: 'user',
                loadChildren: './user/user.module#userModule'
            },
            {
                path: 'project/access_token/:any',
                loadChildren: './project/project.module#projectModule'
            },
            {
                path: '',
                loadChildren: './pages/pages.module#PagesModule',
            }
        ]
    }
];
var AppRoutingModule = (function () {
    function AppRoutingModule() {
    }
    return AppRoutingModule;
}());
AppRoutingModule = __decorate([
    core_1.NgModule({
        imports: [router_1.RouterModule.forRoot(exports.routes)],
        exports: [router_1.RouterModule]
    })
], AppRoutingModule);
exports.AppRoutingModule = AppRoutingModule;
//# sourceMappingURL=/home/admin1/Music/recenceptViewer.com UI/src/app.routing.js.map

/***/ }),

/***/ 539:
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__(0);
var http_1 = __webpack_require__(103);
__webpack_require__(241);
var environment_1 = __webpack_require__(243);
var headers = new http_1.Headers();
headers.append('authorization', localStorage.getItem("auth_token"));
var LayoutService = (function () {
    function LayoutService(http) {
        this.http = http;
    }
    LayoutService.prototype.getData = function (id) {
        return this.http.get(environment_1.environment.baseUrl + "getprojectDetails/" + id)
            .map(function (response) {
            return response.json();
        });
    };
    LayoutService.prototype.addFolder = function (folder, uid, pid) {
        return this.http.post(environment_1.environment.baseUrl + "addFolder/" + pid + "/" + uid + "/", JSON.stringify(folder))
            .map(function (response) {
            return response.json();
        });
    };
    LayoutService.prototype.renameFolder = function (id, obj) {
        return this.http.post(environment_1.environment.baseUrl + "renameFolder/" + id + "/", JSON.stringify(obj))
            .map(function (response) {
            return response.json();
        });
    };
    LayoutService.prototype.deleteFolder = function (id, obj) {
        return this.http.post(environment_1.environment.baseUrl + "deleteFolder/" + id + "/", JSON.stringify(obj))
            .map(function (response) {
            return response.json();
        });
    };
    LayoutService.prototype.addNewTextList = function (uid, pid, fid, access_token, obj) {
        var formData = new FormData();
        for (var i in obj) {
            formData.append('text_file' + i, obj[i]);
        }
        console.log("--------------------");
        console.log(obj);
        console.log("--------------------");
        //  console.log(obj)
        // 	for(var i in obj){
        // 		console.log(obj[i])
        // 	}
        //  formData.append('text_file'+i,obj[i]);
        var headers = new http_1.Headers();
        headers.append('Content-Type', 'multipart/form-data');
        headers.append('Accept', 'application/json');
        var options = new http_1.RequestOptions({ headers: headers });
        console.log(formData);
        return this.http.post(environment_1.environment.baseUrl + "Savetextlist/" + pid + "/" + uid + "/" + fid + "/" + access_token + "/", formData)
            .map(function (response) {
            return response.json();
        });
    };
    LayoutService.prototype.addNewConceptList = function (folder_id, obj) {
        var formData = new FormData();
        formData.append('browse_concept_list', obj);
        var headers = new http_1.Headers();
        headers.append('Content-Type', 'multipart/form-data');
        headers.append('Accept', 'application/json');
        var options = new http_1.RequestOptions({ headers: headers });
        return this.http.post(environment_1.environment.baseUrl + "saveConceptlist/" + localStorage.getItem('projectId') + "/" + localStorage.getItem('userId') + "/" + folder_id + "/", formData)
            .map(function (response) {
            return response.json();
        });
    };
    LayoutService.prototype.addNewStopList = function (obj) {
        var formData = new FormData();
        formData.append('browse_stop_list', obj);
        var headers = new http_1.Headers();
        headers.append('Content-Type', 'multipart/form-data');
        headers.append('Accept', 'application/json');
        var options = new http_1.RequestOptions({ headers: headers });
        return this.http.post(environment_1.environment.baseUrl + "import-stop-list/", formData)
            .map(function (response) {
            return response.json();
        });
    };
    LayoutService.prototype.deleteConceptList = function (obj) {
        return this.http.post(environment_1.environment.baseUrl + "deleteconcept/", JSON.stringify(obj))
            .map(function (response) {
            return response.json();
        });
    };
    LayoutService.prototype.deleteStopList = function (obj) {
        return this.http.post(environment_1.environment.baseUrl + "delete-stoplist/" + obj + "/", JSON.stringify(obj))
            .map(function (response) {
            return response.json();
        });
    };
    LayoutService.prototype.renameConceptList = function (obj) {
        return this.http.post(environment_1.environment.baseUrl + "renameconcept/", JSON.stringify(obj))
            .map(function (response) {
            return response.json();
        });
    };
    LayoutService.prototype.renameStopList = function (obj) {
        return this.http.post(environment_1.environment.baseUrl + "rename-stoplist/", JSON.stringify(obj))
            .map(function (response) {
            return response.json();
        });
    };
    LayoutService.prototype.getstoplist = function () {
        return this.http.get(environment_1.environment.baseUrl + "stoplistdata/")
            .map(function (response) {
            return response.json();
        });
    };
    LayoutService.prototype.addDefination = function (obj) {
        return this.http.post(environment_1.environment.baseUrl + "addDefination/", JSON.stringify(obj))
            .map(function (response) {
            return response.json();
        });
    };
    LayoutService.prototype.getDefination = function (id) {
        return this.http.get(environment_1.environment.baseUrl + "getDefination/" + id + "/")
            .map(function (response) {
            return response.json();
        });
    };
    LayoutService.prototype.deleteTextList = function (obj) {
        return this.http.post(environment_1.environment.baseUrl + "delete-text-list/" + obj.id + "/", JSON.stringify(obj))
            .map(function (response) {
            return response.json();
        });
    };
    LayoutService.prototype.getConceptlist = function (pid) {
        return this.http.get(environment_1.environment.baseUrl + "getconceptlist/" + pid + "/")
            .map(function (response) {
            return response.json();
        });
    };
    LayoutService.prototype.deleteMultipleConceptList = function (obj) {
        return this.http.post(environment_1.environment.baseUrl + "delete-multiple-concept/", JSON.stringify(obj))
            .map(function (response) {
            return response.json();
        });
    };
    LayoutService.prototype.addsubFolder = function (uid, pid, obj) {
        return this.http.post(environment_1.environment.baseUrl + "addsubFolder/" + uid + "/" + pid + "/", JSON.stringify(obj))
            .map(function (response) {
            return response.json();
        });
    };
    LayoutService.prototype.addConceptFolder = function (folder, uid, pid) {
        return this.http.post(environment_1.environment.baseUrl + "create-folder/" + pid + "/" + uid + "/", JSON.stringify(folder))
            .map(function (response) {
            return response.json();
        });
    };
    LayoutService.prototype.getConceptFolder = function (pid) {
        return this.http.get(environment_1.environment.baseUrl + "get-concept-folder/" + pid)
            .map(function (response) {
            return response.json();
        });
    };
    LayoutService.prototype.deleteConceptFolder = function (id) {
        return this.http.post(environment_1.environment.baseUrl + "delete-concept-folder/" + id.id + "/", JSON.stringify(id))
            .map(function (response) {
            return response.json();
        });
    };
    LayoutService.prototype.renameFolderConcept = function (id, obj) {
        return this.http.post(environment_1.environment.baseUrl + "rename-concept-folder/" + id + "/", JSON.stringify(obj))
            .map(function (response) {
            return response.json();
        });
    };
    LayoutService.prototype.addUrl = function (pid, obj) {
        return this.http.post(environment_1.environment.baseUrl + "add-url/" + pid + "/", JSON.stringify(obj))
            .map(function (response) {
            return response.json();
        });
    };
    LayoutService.prototype.deleteURL = function (obj) {
        return this.http.post(environment_1.environment.baseUrl + "delete-url/" + obj.id + "/", JSON.stringify(obj))
            .map(function (response) {
            return response.json();
        });
    };
    LayoutService.prototype.addNewMasterList = function (obj) {
        var formData = new FormData();
        formData.append('text_file', obj);
        var headers = new http_1.Headers();
        headers.append('Content-Type', 'multipart/form-data');
        headers.append('Accept', 'application/json');
        var options = new http_1.RequestOptions({ headers: headers });
        return this.http.post(environment_1.environment.baseUrl + "import-master-list/" + localStorage.getItem('userId') + "/", formData)
            .map(function (response) {
            return response.json();
        });
    };
    LayoutService.prototype.getmasterlist = function () {
        return this.http.get(environment_1.environment.baseUrl + "get-master-list/")
            .map(function (response) {
            return response.json();
        });
    };
    LayoutService.prototype.deleteMasterList = function (obj) {
        return this.http.get(environment_1.environment.baseUrl + "delete-master-list/" + obj)
            .map(function (response) {
            return response.json();
        });
    };
    LayoutService.prototype.renameMasterList = function (obj) {
        return this.http.post(environment_1.environment.baseUrl + "rename-master-list/", JSON.stringify(obj))
            .map(function (response) {
            return response.json();
        });
    };
    LayoutService.prototype.getKnowledgeNode = function () {
        return this.http.get("https://mattersmith1.embeddedexperience.com/api/graph")
            .map(function (response) {
            return response.json();
        });
    };
    return LayoutService;
}());
LayoutService = __decorate([
    core_1.Injectable(),
    __metadata("design:paramtypes", [typeof (_a = typeof http_1.Http !== "undefined" && http_1.Http) === "function" && _a || Object])
], LayoutService);
exports.LayoutService = LayoutService;
var _a;
//# sourceMappingURL=/home/admin1/Music/recenceptViewer.com UI/src/layouts.service.js.map

/***/ }),

/***/ 540:
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__(0);
/**
* Allows the aside to be toggled via click.
*/
var AsideToggleDirective = (function () {
    function AsideToggleDirective() {
    }
    AsideToggleDirective.prototype.toggleOpen = function ($event) {
        $event.preventDefault();
        document.querySelector('body').classList.toggle('aside-menu-hidden');
    };
    return AsideToggleDirective;
}());
__decorate([
    core_1.HostListener('click', ['$event']),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", void 0)
], AsideToggleDirective.prototype, "toggleOpen", null);
AsideToggleDirective = __decorate([
    core_1.Directive({
        selector: '.aside-menu-toggler',
    }),
    __metadata("design:paramtypes", [])
], AsideToggleDirective);
exports.AsideToggleDirective = AsideToggleDirective;
//# sourceMappingURL=/home/admin1/Music/recenceptViewer.com UI/src/aside.directive.js.map

/***/ }),

/***/ 541:
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__(0);
var router_1 = __webpack_require__(156);
__webpack_require__(416);
var BreadcrumbsComponent = (function () {
    function BreadcrumbsComponent(router, route) {
        this.router = router;
        this.route = route;
    }
    BreadcrumbsComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.router.events.filter(function (event) { return event instanceof router_1.NavigationEnd; }).subscribe(function (event) {
            _this.breadcrumbs = [];
            var currentRoute = _this.route.root, url = '';
            do {
                var childrenRoutes = currentRoute.children;
                currentRoute = null;
                childrenRoutes.forEach(function (route) {
                    if (route.outlet === 'primary') {
                        var routeSnapshot = route.snapshot;
                        url += '/' + routeSnapshot.url.map(function (segment) { return segment.path; }).join('/');
                        _this.breadcrumbs.push({
                            label: route.snapshot.data,
                            url: url
                        });
                        currentRoute = route;
                    }
                });
            } while (currentRoute);
        });
    };
    return BreadcrumbsComponent;
}());
BreadcrumbsComponent = __decorate([
    core_1.Component({
        selector: 'breadcrumbs',
        template: "\n  <template ngFor let-breadcrumb [ngForOf]=\"breadcrumbs\" let-last = last>\n    <li class=\"breadcrumb-item\" *ngIf=\"breadcrumb.label.title&&breadcrumb.url.substring(breadcrumb.url.length-1) == '/' || breadcrumb.label.title&&last\" [ngClass]=\"{active: last}\">\n      <a *ngIf=\"!last\" [routerLink]=\"breadcrumb.url\">{{breadcrumb.label.title}}</a>\n      <span *ngIf=\"last\" [routerLink]=\"breadcrumb.url\">{{breadcrumb.label.title}}</span>\n    </li>\n  </template>"
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof router_1.Router !== "undefined" && router_1.Router) === "function" && _a || Object, typeof (_b = typeof router_1.ActivatedRoute !== "undefined" && router_1.ActivatedRoute) === "function" && _b || Object])
], BreadcrumbsComponent);
exports.BreadcrumbsComponent = BreadcrumbsComponent;
var _a, _b;
//# sourceMappingURL=/home/admin1/Music/recenceptViewer.com UI/src/breadcrumb.component.js.map

/***/ }),

/***/ 542:
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__(0);
var NavDropdownDirective = (function () {
    function NavDropdownDirective(el) {
        this.el = el;
    }
    NavDropdownDirective.prototype.toggle = function () {
        this.el.nativeElement.classList.toggle('open');
    };
    return NavDropdownDirective;
}());
NavDropdownDirective = __decorate([
    core_1.Directive({
        selector: '.nav-dropdown'
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof core_1.ElementRef !== "undefined" && core_1.ElementRef) === "function" && _a || Object])
], NavDropdownDirective);
exports.NavDropdownDirective = NavDropdownDirective;
/**
* Allows the dropdown to be toggled via click.
*/
var NavDropdownToggleDirective = (function () {
    function NavDropdownToggleDirective(dropdown) {
        this.dropdown = dropdown;
    }
    NavDropdownToggleDirective.prototype.toggleOpen = function ($event) {
        $event.preventDefault();
        this.dropdown.toggle();
    };
    return NavDropdownToggleDirective;
}());
__decorate([
    core_1.HostListener('click', ['$event']),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", void 0)
], NavDropdownToggleDirective.prototype, "toggleOpen", null);
NavDropdownToggleDirective = __decorate([
    core_1.Directive({
        selector: '.nav-dropdown-toggle',
    }),
    __metadata("design:paramtypes", [NavDropdownDirective])
], NavDropdownToggleDirective);
exports.NavDropdownToggleDirective = NavDropdownToggleDirective;
exports.NAV_DROPDOWN_DIRECTIVES = [NavDropdownDirective, NavDropdownToggleDirective];
var _a;
// export const NGB_DROPDOWN_DIRECTIVES = [NgbDropdownToggle, NgbDropdown];
//# sourceMappingURL=/home/admin1/Music/recenceptViewer.com UI/src/nav-dropdown.directive.js.map

/***/ }),

/***/ 543:
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__(0);
/**
* Allows the sidebar to be toggled via click.
*/
var SidebarToggleDirective = (function () {
    function SidebarToggleDirective() {
    }
    SidebarToggleDirective.prototype.toggleOpen = function ($event) {
        $event.preventDefault();
        document.querySelector('body').classList.toggle('sidebar-hidden');
    };
    return SidebarToggleDirective;
}());
__decorate([
    core_1.HostListener('click', ['$event']),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", void 0)
], SidebarToggleDirective.prototype, "toggleOpen", null);
SidebarToggleDirective = __decorate([
    core_1.Directive({
        selector: '.sidebar-toggler',
    }),
    __metadata("design:paramtypes", [])
], SidebarToggleDirective);
exports.SidebarToggleDirective = SidebarToggleDirective;
var MobileSidebarToggleDirective = (function () {
    function MobileSidebarToggleDirective() {
    }
    // Check if element has class
    MobileSidebarToggleDirective.prototype.hasClass = function (target, elementClassName) {
        return new RegExp('(\\s|^)' + elementClassName + '(\\s|$)').test(target.className);
    };
    MobileSidebarToggleDirective.prototype.toggleOpen = function ($event) {
        $event.preventDefault();
        document.querySelector('body').classList.toggle('sidebar-mobile-show');
    };
    return MobileSidebarToggleDirective;
}());
__decorate([
    core_1.HostListener('click', ['$event']),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", void 0)
], MobileSidebarToggleDirective.prototype, "toggleOpen", null);
MobileSidebarToggleDirective = __decorate([
    core_1.Directive({
        selector: '.mobile-sidebar-toggler',
    }),
    __metadata("design:paramtypes", [])
], MobileSidebarToggleDirective);
exports.MobileSidebarToggleDirective = MobileSidebarToggleDirective;
/**
* Allows the off-canvas sidebar to be closed via click.
*/
var SidebarOffCanvasCloseDirective = (function () {
    function SidebarOffCanvasCloseDirective() {
    }
    // Check if element has class
    SidebarOffCanvasCloseDirective.prototype.hasClass = function (target, elementClassName) {
        return new RegExp('(\\s|^)' + elementClassName + '(\\s|$)').test(target.className);
    };
    // Toggle element class
    SidebarOffCanvasCloseDirective.prototype.toggleClass = function (elem, elementClassName) {
        var newClass = ' ' + elem.className.replace(/[\t\r\n]/g, ' ') + ' ';
        if (this.hasClass(elem, elementClassName)) {
            while (newClass.indexOf(' ' + elementClassName + ' ') >= 0) {
                newClass = newClass.replace(' ' + elementClassName + ' ', ' ');
            }
            elem.className = newClass.replace(/^\s+|\s+$/g, '');
        }
        else {
            elem.className += ' ' + elementClassName;
        }
    };
    SidebarOffCanvasCloseDirective.prototype.toggleOpen = function ($event) {
        $event.preventDefault();
        if (this.hasClass(document.querySelector('body'), 'sidebar-off-canvas')) {
            this.toggleClass(document.querySelector('body'), 'sidebar-opened');
        }
    };
    return SidebarOffCanvasCloseDirective;
}());
__decorate([
    core_1.HostListener('click', ['$event']),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", void 0)
], SidebarOffCanvasCloseDirective.prototype, "toggleOpen", null);
SidebarOffCanvasCloseDirective = __decorate([
    core_1.Directive({
        selector: '.sidebar-close',
    }),
    __metadata("design:paramtypes", [])
], SidebarOffCanvasCloseDirective);
exports.SidebarOffCanvasCloseDirective = SidebarOffCanvasCloseDirective;
exports.SIDEBAR_TOGGLE_DIRECTIVES = [SidebarToggleDirective, SidebarOffCanvasCloseDirective, MobileSidebarToggleDirective];
//# sourceMappingURL=/home/admin1/Music/recenceptViewer.com UI/src/sidebar.directive.js.map

/***/ }),

/***/ 719:
/***/ (function(module, exports) {

module.exports = "\n<header class=\"app-header navbar\">\n  <style>\n    \n/* Popup container - can be anything you want */\n \n.sidebar .nav .nav-item.nav-dropdown.open > ul, .sidebar .nav .nav-item.nav-dropdown.open > ol {    max-height: 15000px !important;}\n\n.textmenu,subMenuForFolders{\n  display:none;\n  background-color:#a0938f;\n  border-radius: 6px;\n  color:white;\n  text-align: left;\n      z-index: 37;\n    margin-left:85px;\n    position: fixed;\n    width:140px;\n    padding-left:15px;\n    padding-right:15px;\n    padding-top:10px;\n    padding-bottom:10px;\n        top: 29%;\n     font-size:12px;\n}\np{\n    width: 10em;  \n    word-wrap: break-word;\n}\np:hover {\n    background-color: #996600;\n}\n.loader {\n  border: 5px solid #f3f3f3;\n  border-radius: 50%;\n  border-top: 5px solid black;\n  border-right: 5px solid black;\n  border-bottom: 5px solid black;\n  width: 50px;\n  height: 50px;\n  -webkit-animation: spin 2s linear infinite; /* Safari */\n  animation: spin 2s linear infinite;\n}\n\n/* Safari */\n@-webkit-keyframes spin {\n  0% { -webkit-transform: rotate(0deg); }\n  100% { -webkit-transform: rotate(360deg); }\n}\n\n@keyframes spin {\n  0% { transform: rotate(0deg); }\n  100% { transform: rotate(360deg); }\n}\n\n.context-menu-p{\n      margin-bottom: 3px;\n}\n\n</style>\n \n  <button class=\"navbar-toggler mobile-sidebar-toggler hidden-lg-up\" type=\"button\">&#9776;</button>\n  <a class=\"\" href=\"#\"></a>\n  \n  <ul class=\"nav navbar-nav hidden-md-down\">\n    <li class=\"nav-item\">\n      <a  style=\"color:white;position: absolute;margin-left: 200px;margin-top: -6px\" id=\"toglerbtn\" class=\"nav-link navbar-toggler sidebar-toggler\" href=\"#\">&#9776;</a>\n    </li>\n    <!--<b style=\"font-size:19px;color:black;\"> RepindexApp</b>-->\n     <img src=\"assets/img/LOG.png\" style=\"width: 200px;\n    height: 54px;margin-left:-50px;\" >\n  \n  </ul>\n  <ul class=\"nav navbar-nav ml-auto\">\n   \n    <li class=\"nav-item dropdown\" dropdown (onToggle)=\"toggled($event)\">\n      <a style=\"color:black;\" class=\"nav-link dropdown-toggle\" data-toggle=\"dropdown\" href=\"#\" role=\"button\" aria-haspopup=\"true\" aria-expanded=\"false\" dropdownToggle>\n        <span class=\"hidden-md-down\" style=\"color:white;font-size:12px\">{{userName}}</span>\n      </a>\n      <div class=\"dropdown-menu dropdown-menu-right\" dropdownMenu aria-labelledby=\"simple-dropdown\">\n\n        <div class=\"dropdown-header text-center\"><strong>Account</strong></div>\n\n      \n        <a class=\"dropdown-item\" href=\"#\" (click)=\"logout()\"><i class=\"fa fa-lock\"></i> Logout</a>\n      </div>\n    </li>\n    <li class=\"nav-item hidden-md-down\">\n      <a style=\"display:none;\"class=\"nav-link navbar-toggler aside-menu-toggler\" href=\"#\">&#9776;</a>\n    </li>\n  </ul>\n  \n</header>\n\n<div class=\"app-body\" (click)=\"allClose()\"  >\n  <div class=\"sidebar\" >\n    <nav class=\"sidebar-nav\" style=\"background-color:#003746\">\n\n    <div *ngIf=\"username==='admin'\">\n      <ul class=\"nav\">\n          <li class=\"nav-item\">\n            <a class=\"nav-link\" routerLinkActive=\"active\" [routerLink]=\"['/dashboard']\"><i class=\"fa fa-user\"style=\"color:#808080;\"></i>{{'Clients' | translate}}</a>\n          </li>\n          <li class=\"nav-item nav-dropdown\" routerLinkActive=\"open\">\n            <a     (contextmenu)=\"ConceptStopListMenu($event)\" class=\"nav-link nav-dropdown-toggle\" href=\"#\">\n                <i  class=\"fa fa-file-text fa-lg mt-4\" ></i> {{'stop-list'| translate}}\n                <div class=\"textmenu\" id=\"m10\">\n                  <p (click)=\"newConceptStopListImport()\" style=\"cursor:pointer;\"  class=\"context-menu-p\">{{'Import-Stoplist'| translate}}</p>\n                  <p (click)=\"closeStopList()\" style=\"cursor:pointer;\"  class=\"context-menu-p\">Quit</p>\n                </div>\n              \n            </a>\n            <ul>\n                <li  class=\"nav-item nav-dropdown\" *ngFor=\"let child of  getstop\"> \n                  <a class=\"nav-link\" (contextmenu)=\"ConceptStopListMenuChild($event)\" id=\"{{child.fields.name}}@{{child.pk}}\">\n                      <i  class=\"fa fa-long-arrow-right fa-lg mt-4\"></i>\n                      {{child.fields.name}}\n                      <div  class=\"textmenu\" id=\"m11\">\n                        <p (click)=\"renameStopList()\" style=\"cursor:pointer;\"  class=\"context-menu-p\">Rename Stoplist</p>\n                        <p (click)=\"deleteStopList(child.pk)\" style=\"cursor:pointer;\"  class=\"context-menu-p\">Delete Stoplist</p>\n                        <p (click)=\"closeStopListChild()\" style=\"cursor:pointer;\"  class=\"context-menu-p\">Quit</p>\n                          <input type=\"hidden\"  id=\"hiddenId-of-stop-list\">\n                          <input type=\"hidden\"  id=\"textOfStopList\">\n                      </div>\n                  </a>\n                </li>\n            </ul>\n          </li>\n      </ul>\n    </div>\n   <div *ngIf=\"username==='superuser'\">\n   <ul class=\"nav\">\n    <li class=\"nav-item\">\n          <a class=\"nav-link\" routerLinkActive=\"active\" [routerLink]=\"['/superdashboard']\"><i class=\"fa fa-user\"style=\"color:#ffffff;\"></i>Admins</a>\n        </li>\n\n     </ul>\n     <ul class=\"nav\">\n      <li class=\"nav-item\" class=\"nav-item nav-dropdown\"  routerLinkActive=\"open\">\n            <a class=\"nav-link\" class=\"nav-link nav-dropdown-toggle\" (contextmenu)=\"showMasterList($event)\" routerLinkActive=\"active\" >\n                <i class=\"fa fa-book\"style=\"color:#ffffff\">\n                  </i>masterlist\n            </a>\n            <div class=\"textmenu\" id=\"masterlist\">\n              <p (click)=\"newMasterListImport()\" style=\"cursor:pointer;\"  class=\"context-menu-p\">Import MasterList</p>\n              <p (click)=\"closeMasterList()\" style=\"cursor:pointer;\"  class=\"context-menu-p\">Quit</p>\n            </div>\n              <ul>               \n                  <li *ngFor=\"let master  of masterConcepts\" class=\"nav-item nav-dropdown\" routerLinkActive=\"open\">\n                    <a class=\"nav-link\" class=\"nav-link nav-dropdown-toggle\" (contextmenu)=\"MasterListMenuChild($event)\" id=\"{{master.id}}\">\n                      <i  class=\"fa fa-file-word-o fa-lg mt-4\"></i>\n                         {{master.name}}\n                    </a>\n                    <div  class=\"textmenu\" id=\"masterChileId\">\n                      <a (click)=\"renameMasterList()\" style=\"cursor:pointer;\"  class=\"context-menu-p\">Rename Masterlist</a>\n                      <p (click)=\"deleteMasterList()\" style=\"cursor:pointer;\"  class=\"context-menu-p\">Delete Masterlist</p>\n                      <p (click)=\"closemasterChild()\" style=\"cursor:pointer;\"  class=\"context-menu-p\">Quit</p>\n                        <input type=\"hidden\"  id=\"hiddenId-of-master-list\">\n                        <input type=\"hidden\"  id=\"textOfMasterList\">\n                    </div>\n                    <ul>\n                      <li *ngFor=\"let mas  of master.values\" class=\"nav-item nav-dropdown\" routerLinkActive=\"open\">\n                          <a class=\"nav-link\" class=\"nav-link nav-dropdown-toggle\">\n                            <i  class=\"fa fa-arrows-v fa-lg mt-4\"></i>\n                              {{mas.umbrella}}\n                          </a>\n                    <ul>\n                      <li *ngFor=\"let ma  of mas.lst_terms\" class=\"nav-item nav-dropdown\" routerLinkActive=\"open\">\n                          <a class=\"nav-link\">\n                            <i  class=\"fa fa-long-arrow-right fa-lg mt-4\"></i>\n                              {{ma}}\n                          </a>\n                      </li>\n                    </ul>\n                      </li>\n                    </ul>\n                  </li>\n          </ul>\n      </li>\n     </ul>\n   </div>\n\n\n\n   <div *ngIf=\"username==='user'\">\n    \n   <ul class=\"nav\">\n       <!--Project Name-->\n          <li class=\"nav-item nav-dropdown\" routerLinkActive=\"open\">\n          <a class=\"nav-link nav-dropdown-toggle\"  style=\"font-size:12px;\" href=\"#\"><i class=\"fa fa-book fa-lg mt-4\"></i>{{projectName}}</a>\n          \n        </li>\n\n         <!--Project Name-->\n          <li class=\"nav-item nav-dropdown\" routerLinkActive=\"open\">\n          <a style=\"font-size:12px;\" class=\"nav-link nav-dropdown-toggle\" href=\"#\"><i class=\"fa fa-search-plus fa-lg mt-4\"></i> {{'Query'| translate }}</a>\n          <ul class=\"nav-dropdown-items\">\n            <li class=\"nav-item\">\n              <a class=\"nav-link\" style=\"font-size:12px;\" (click)=\"newQueryPgae()\"><i class=\"icon-puzzle\"></i>{{'New-Query'| translate }}</a>\n            </li>\n          \n          </ul>\n        </li>\n         <!--Query-->\n \n          <!--TEXT LIST-->\n          <li class=\"nav-item nav-dropdown\" routerLinkActive=\"open\">\n              <a style=\"font-size:12px;\" (contextmenu)=\"demo1($event)\" class=\"nav-link nav-dropdown-toggle\" href=\"#\">\n                <i  class=\"fa fa-file-text fa-lg mt-4\" ></i> {{'text-list'| translate }}\n              </a>\n              <div class=\"textmenu\" id=\"m1\">\n                <p  (click)=\"addNewFolderToTextList()\" style=\"cursor:pointer;\"  class=\"context-menu-p\"> {{'New-folder'| translate }}</p>\n                <p   (click)=\"closem1()\" style=\"cursor:pointer;\"  class=\"context-menu-p\"> {{'Quit'| translate }}</p>\n              </div>\n                <ul class=\"nav-dropdown-items\">\n                  <!--*ngFor=\"let folder  of textlistfolders\"-->\n                    <li  *ngFor=\"let folder  of text_folders\" class=\"nav-item nav-dropdown\">\n                        <a style=\"font-size:12px;cursor:pointer;\" *ngIf=\"folder.fields.parent_folder_id == null\"  (contextmenu)=\"demo3($event)\" class=\"nav-link nav-dropdown-toggle\" id=\"{{folder.pk}}@{{folder.fields.name}}\"  >\n                              <i class=\"fa fa-folder fa-lg mt-4\" style=\"font-size:12px;margin-left: 0px;color:yellow;cursor:pointer;\"></i>\n                              {{folder.fields.name}}           \n                        </a>\n                        <div class=\"textmenu\" id=\"m3\">\n                            <p (click)=\"addNewTextList()\"style=\"cursor:pointer;\" class=\"context-menu-p\">{{'Add-textlist'| translate }}</p>\n                            <p (click)=\"importUrl()\" style=\"cursor:pointer;\" class=\"context-menu-p\">Import URL</p>\n                            <p (click)=\"addSubfolder()\"style=\"cursor:pointer;\" class=\"context-menu-p\">Add NewFolder</p>\n                            <p (click)=\"renameTextListFolder()\" style=\"cursor:pointer;\"  class=\"context-menu-p\">{{'rename-folder'| translate }}</p>\n                            <p (click)=\"deleteTextListFolder()\" style=\"cursor:pointer;\" class=\"context-menu-p\">{{'delete-folder'| translate }}</p>\n                            <p (click)=\"closem3()\" style=\"cursor:pointer;\"  class=\"context-menu-p\">{{'Quit'| translate }}</p>\n                            <input type=\"hidden\"  id=\"hiddenId-of\">\n                            <input type=\"hidden\"  id=\"textOfFolder\">\n                        </div> \n                        <ul class=\"nav-dropdown-items\" >\n                          <li  class=\"nav-item nav-dropdown\" *ngFor=\"let list of text_list_data\">\n                              <a style=\"font-size:12px\" *ngIf=\"folder.pk == list.fields.folder_id\" class=\"nav-link  \" (contextmenu)=\"TextListMenuChild($event,list)\" id=\"{{list.pk}}\">\n                                <i  class=\"fa fa-file-word-o fa-lg mt-4\"></i>\n                                {{list.fields.name}}\n                                <div  class=\"textmenu\" id=\"p1\">\n                                    <p (click)=\"deleteTextListData()\" style=\"cursor:pointer;\"  class=\"context-menu-p\">Delete Text</p>\n                                    <p (click)=\"closeTxtList()\" style=\"cursor:pointer;\"  class=\"context-menu-p\">Quit</p>\n                                    <input type=\"hidden\"  id=\"hiddenId-of-text-list\" >\n                                    <input type=\"hidden\"  id=\"textListId\">\n                                </div>\n                            </a>\n                        </li>\n\n\n\n\n                         <li  class=\"nav-item nav-dropdown\" *ngFor=\"let ul of urls\">\n                              <a *ngIf=\"folder.pk == ul.fields.folder_id\" class=\"nav-link\" (contextmenu)=\"TextListUrl($event,ul.pk)\" id=\"{{ul.pk}}\">\n                                 <i style=\"color:aqua;\" class=\"fa fa-external-link fa-4\"></i>\n                                  <font color=\"aqua\"> {{ul.fields.name}}.url </font>\n                                        <div  class=\"textmenu\" id=\"url_menu\">\n                                            <p (click)=\"deleteTextListUrl()\" style=\"cursor:pointer;\"  class=\"context-menu-p\">Delete URL</p>\n                                            <p (click)=\"closeurl()\" style=\"cursor:pointer;\"  class=\"context-menu-p\">Quit</p>\n                                            <input type=\"hidden\"  id=\"hiddenId-of-text-list-url\" >\n                                            <input type=\"hidden\"  id=\"textListurlId\">\n                                        </div>\n                             </a>\n                        </li>\n\n                          <li  *ngFor=\"let fold  of text_folders\" class=\"nav-item nav-dropdown\">\n                            <a style=\"font-size:12px\" *ngIf=\"fold.fields.parent_folder_id == folder.pk\"   (contextmenu)=\"demos3($event)\" class=\"nav-link nav-dropdown-toggle\" id=\"{{fold.pk}}@{{fold.fields.name}}\"  >\n                                <i class=\"fa fa-folder fa-lg mt-4\" style=\"font-size:12px;margin-left: 10px;color:cornflowerblue;cursor:pointer;\"></i>\n                                    {{fold.fields.name}}            \n                            </a>\n                            <div class=\"textmenu\" id=\"mm3\"> \n                                <p (click)=\"addNewSubTextList()\"style=\"cursor:pointer;\" class=\"context-menu-p\">{{'Add-textlist'| translate }}</p>\n                                <p (click)=\"renameSubTextListFolder()\" style=\"cursor:pointer;\"  class=\"context-menu-p\">{{'rename-folder'| translate }}</p>\n                                <p (click)=\"deleteSubTextListFolder()\"  class=\"context-menu-p\">{{'delete-folder'| translate }}</p>\n                                <p (click)=\"closem3()\" style=\"cursor:pointer;\"  class=\"context-menu-p\">{{'Quit'| translate }}</p>\n                                <input type=\"hidden\"  id=\"hiddenId-ofsubfolder\">\n                                <input type=\"hidden\"  id=\"textOfsubFolder\">\n                            </div>\n                            <ul class=\"nav-dropdown-items\" >\n                                  <li  class=\"nav-item nav-dropdown\" *ngFor=\"let list of text_list_data\">\n                                      <a style=\"font-size:12px\" style=\"font-size:12px\" *ngIf=\"fold.pk == list.fields.folder_id\" class=\"nav-link  \" (contextmenu)=\"TextListMenuChild($event,list)\" id=\"{{list.pk}}\">\n                                        <i  class=\"fa fa-file-word-o fa-lg mt-4\"></i>\n                                        {{list.fields.name}}\n                                        <div  class=\"textmenu\" id=\"p1\">\n                                            <p (click)=\"deleteTextListData()\" style=\"cursor:pointer;\"  class=\"context-menu-p\">Delete Text</p>\n                                            <p (click)=\"closeTxtList()\" style=\"cursor:pointer;\"  class=\"context-menu-p\">Quit</p>\n                                            <input type=\"hidden\"  id=\"hiddenId-of-text-list\" >\n                                            <input type=\"hidden\"  id=\"textListId\">\n                                        </div>\n                                    </a>\n                                </li>\n                            </ul>\n                          </li>\n                      </ul>  \n                   </li>\n                </ul>\n          </li>\n       \n\n<!-- Concept List -->\n<li class=\"nav-item nav-dropdown\" routerLinkActive=\"open\">\n   <a  style=\"font-size:12px;\" (contextmenu)=\"demo2($event)\" class=\"nav-link nav-dropdown-toggle\" href=\"#\" >\n      <i class=\"fa fa-list fa-lg mt-4\"></i> \n      {{'Concept-list'| translate }}\n   </a>\n   <div class=\"textmenu\" id=\"m2\">\n      <p  (click)=\"addNewFolderToConceptList()\" style=\"cursor:pointer;\"  class=\"context-menu-p\"> {{'New-folder'| translate }} </p>\n      <p (click)=\"deletemultiple()\" style=\"cursor:pointer;\"  class=\"context-menu-p\">Delete Multiple</p>\n      <p   (click)=\"closem1()\" style=\"cursor:pointer;\"  class=\"context-menu-p\"> {{'Quit'| translate }}</p>\n   </div> \n   <ul class=\"nav-dropdown-items\">\n      <li *ngFor=\"let fold  of conceptFolder\" class=\"nav-item nav-dropdown\">\n         <!--<a (contextmenu)=\"showConceptListMenu($event)\" class=\"nav-link nav-dropdown-toggle\" id=\"{{conceptlist.name}}@{{conceptlist.id}}\">\n              <i class=\"fa fa-file-text fa-lg mt-4;\"></i>\n              {{conceptlist.name}} \n         </a>-->\n         <!--foldername-->\n           <a style=\"font-size:12px;cursor:pointer;\" (contextmenu)=\"demo4($event)\" class=\"nav-link nav-dropdown-toggle\" id=\"{{fold.pk}}@{{fold.fields.name}}\"  >\n                  <i class=\"fa fa-folder fa-lg mt-4\" style=\"font-size:12px;margin-left: 0px;color:yellow;cursor:pointer;\"></i>\n                  {{fold.fields.name}}\n          </a> \n          <!--foldername-->\n           <div class=\"textmenu\" id=\"mm5\">\n                <p (click)=\"newConceptListImport()\" style=\"cursor:pointer;\"  class=\"context-menu-p\">{{'import-csv'| translate }}</p>\n                <p (click)=\"renameConceptListFolder()\" style=\"cursor:pointer;\"  class=\"context-menu-p\">{{'rename-folder'| translate }}</p>\n                <p  (click)=\"deleteConceptListFolder()\" style=\"cursor:pointer;\"  class=\"context-menu-p\">{{'delete-folder'| translate }}</p>\n                <p (click)=\"closem4()\" style=\"cursor:pointer;\"  class=\"context-menu-p\">{{'Quit'| translate }}</p>\n                <input type=\"hidden\"  id=\"hiddenId-of-concept-list-folder-id\">\n                <input type=\"hidden\"  id=\"textOfConceptListFolder\">\n           </div>\n           <ul style=\"\">\n               \n               <li *ngFor=\"let conceptlist of concept\" class=\"nav-item nav-dropdown\">\n                         \n                        <a style=\"font-size:12px\" *ngIf=\"conceptlist.folder_id === fold.pk\" (contextmenu)=\"showConceptListMenu($event)\" class=\"nav-link nav-dropdown-toggle\" id=\"{{conceptlist.name}}@{{conceptlist.id}}\">\n                            <i class=\"fa fa-file-text fa-lg mt-4;\"></i>\n                            {{conceptlist.name}}\n                        </a>\n              <div *ngIf=\"concept_status === 'true'\" class=\"textmenu\" id=\"m4\">\n                  <p  (click)=\"renameConceptList()\" style=\"cursor:pointer;\"  class=\"context-menu-p\">{{'Rename-concept'| translate }}</p>\n                  <p  (click)=\"deleteConceptList()\" style=\"cursor:pointer;\"  class=\"context-menu-p\">{{'delete-concept'| translate }}</p>\n                  <p (click)=\"closem4()\" style=\"cursor:pointer;\"  class=\"context-menu-p\">{{'Quit'| translate }}</p>\n                  <input type=\"hidden\"  id=\"hiddenId-of-concept-list\">\n                  <input type=\"hidden\"  id=\"textOfConceptList\">\n              </div>\n              <ul>\n                  <li  class=\"nav-item nav-dropdown\" *ngFor=\"let item of conceptlist.values\" >\n                    <a style=\"font-size:12px\" (contextmenu)=\"showConceptListMenuUmbrella($event)\"  id=\"{{item.umbrella}}@{{item.terms}}@{{item.id}}\" class=\"nav-link nav-dropdown-toggle\">\n                    <i  class=\"fa fa-arrows-v fa-lg mt-4\"></i>\n                    {{item.umbrella}}\n                    </a>\n                    <div class=\"textmenu\" id=\"m5\">\n                        <p *ngIf=\"concept_status === 'true'\" (click)=\"selectTerms()\"style=\"cursor:pointer;\"  class=\"context-menu-p\">{{'select-terms'| translate }}</p>\n                        <p (click)=\"insertIntosearch()\"style=\"cursor:pointer;\"  class=\"context-menu-p\">{{'Insertsearch'| translate }}</p>\n                        <p *ngIf=\"concept_status === 'true'\" (click)=\"userComment(item.id)\"style=\"cursor:pointer;\"  class=\"context-menu-p\">{{'addefination'| translate }}</p>\n                         <p *ngIf=\"concept_status === 'true'\" style=\"cursor:pointer;\" (click)=\"gotoKnowledge()\" class=\"context-menu-p\">Go to Knowledge</p>\n                       <!--  <p *ngIf=\"concept_status === 'true'\" (click)=\"insertintoand()\" style=\"cursor:pointer;\"  class=\"context-menu-p\">{{'Insertand'| translate }}</p> -->\n                        <p (click)=\"closeM5()\" style=\"cursor:pointer;\"  class=\"context-menu-p\">{{'Quit'| translate }}</p>\n                        <input type=\"hidden\"  id=\"hidenTermsOfUmbrella\">\n                        <input type=\"hidden\"  id=\"hidenUmbrella\">\n                        <input type=\"hidden\"  id=\"conceptid\">\n                    </div>\n                    <ul *ngIf=\"concept_status === 'true'\">\n                        <li  class=\"nav-item nav-dropdown\" *ngFor=\"let child of  item.lst_terms\">\n                          <a style=\"font-size:12px\" class=\"nav-link  \" (contextmenu)=\"ConceptListMenuChild($event,child)\">\n                              <i  class=\"fa fa-long-arrow-right fa-lg mt-4\"></i>\n                              {{child}}\n                              <div  class=\"textmenu\" id=\"mm9\">\n                                <p (click)=\"insertIntosearchChild()\" style=\"cursor:pointer;\"  class=\"context-menu-p\">{{'Insertsearch'| translate }}</p>\n                                <!-- <p (click)=\"insertintoand()\" style=\"cursor:pointer;\"  class=\"context-menu-p\">{{'Insertand'| translate }}</p> -->\n                                <!-- <p (click)=\"addNewTextList()\"style=\"cursor:pointer;\"  class=\"context-menu-p\"> Rename concept</p>\n                                    <p (click)=\"renameTextListFolder()\" style=\"cursor:pointer;\"  class=\"context-menu-p\">Delete concept</p>-->\n                                <p (click)=\"closeMM9()\" style=\"cursor:pointer;\"  class=\"context-menu-p\">{{'Quit'| translate }}</p>\n                                <input type=\"hidden\"  id=\"hidenTermsOfUmbrellaChild\">\n                                <input type=\"hidden\"  id=\"hidenUmbrellaChild\">\n                              </div>\n                          </a>\n                        </li>\n                    </ul>\n                  </li>\n              </ul>\n      </li>\n         </ul>\n      </li>\n   </ul>\n</li>\n        </ul>\n   </div>\n   \n    <!--<div *ngIf=\"project==='addproject'\">\n    \n   </div>-->\n   \n\n\n     \n    </nav>\n  </div>\n\n  <!-- Main content -->\n  <main class=\"main\">\n\n \n\n    <div class=\"container-fluid\">\n      <router-outlet></router-outlet>\n    </div><!-- /.conainer-fluid -->\n  </main>\n\n  <!--ADD FOLDER-->\n\n   <button id=\"uniq\"  hidden type=\"button\" class=\"btn btn-secondary\" data-toggle=\"modal\" (click)=\"myModal.show()\">\n            Launch demo modal\n          </button>\n<div bsModal #myModal=\"bs-modal\" class=\"modal fade\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\" aria-hidden=\"true\">\n  <div class=\"modal-dialog\" role=\"document\">\n    <div class=\"modal-content\">\n      <div class=\"modal-header\">\n          <h6 class=\"modal-title\" id=\"exampleModalLabel\" style=font-size:15px;>{{'New-folder'| translate }}</h6>\n        <button type=\"button\" class=\"close\" (click)=\"myModal.hide()\" aria-label=\"Close\">\n          <span aria-hidden=\"true\">&times;</span>\n        </button>\n      </div>\n      <div class=\"modal-body\">\n        <input type=\"text\" class=\"form-control\" id=\"text-list-new-folder-name\" placeholder=\"Add new folder\">  \n      </div>\n      <div class=\"modal-footer\">\n        <button type=\"button\" class=\"btn btn-secondary\" (click)=\"myModal.hide()\">{{'close'| translate }}</button>\n        <button type=\"button\" class=\"btn btn-primary\" (click)=\"createNewFolder()\" style=background-color:#808080;color:white;>{{'Save'| translate }}</button>\n      </div>\n    </div><!-- /.modal-content -->\n  </div><!-- /.modal-dialog -->\n</div><!-- /.modal -->\n\n\n\n<button id=\"clist\"  hidden type=\"button\" class=\"btn btn-secondary\" data-toggle=\"modal\" (click)=\"mynewModal.show()\">\n     Launch demo modal\n</button>\n<div bsModal #mynewModal=\"bs-modal\" class=\"modal fade\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"mynewModalLabel\" aria-hidden=\"true\">\n  <div class=\"modal-dialog\" role=\"document\">\n    <div class=\"modal-content\">\n      <div class=\"modal-header\">\n          <h6 class=\"modal-title\" id=\"exampleModalLabel\" style=font-size:15px;>{{'New-folder'| translate }}</h6>\n          <button type=\"button\" class=\"close\" (click)=\"mynewModal.hide()\" aria-label=\"Close\">\n            <span aria-hidden = \"true\"> &times; </span>\n          </button>\n      </div>\n      <div class=\"modal-body\">\n        <input type=\"text\" class=\"form-control\" id=\"concept-list-new-folder-name\" placeholder=\"Add new folder\">  \n      </div>\n      <div class=\"modal-footer\">\n        <button type=\"button\" class=\"btn btn-secondary\" (click)=\"mynewModal.hide()\">{{'close'| translate }}</button>\n        <button type=\"button\" class=\"btn btn-primary\" (click)=\"createNewFolderConceptList()\" style=background-color:#808080;color:white;>{{'Save'| translate }}</button>\n      </div>\n    </div><!-- /.modal-content -->\n  </div><!-- /.modal-dialog -->\n</div><!-- /.modal -->\n\n\n\n<!--delete multipl text-->\n<button id=\"deletemultiple\"  hidden type=\"button\" class=\"btn btn-secondary\" data-toggle=\"modal\" (click)=\"myModal21.show()\">\n            Launch demo modal\n          </button>\n<div bsModal #myModal21=\"bs-modal\" class=\"modal fade\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\" aria-hidden=\"true\">\n  <div class=\"modal-dialog\" role=\"document\">\n    <div class=\"modal-content\">\n      <div class=\"modal-header\">\n          <h6 class=\"modal-title\" id=\"exampleModalLabel\" style=font-size:15px;>Delete Multiple files </h6>\n        <button type=\"button\" class=\"close\" (click)=\"myModal21.hide()\" aria-label=\"Close\">\n          <span aria-hidden=\"true\">&times;</span>\n        </button>\n      </div>\n      <div class=\"modal-body\">\n      <div id=\"termsvalue\"> <span *ngFor=\"let list of multipleconceptList\"> \n   \n    <input type=\"checkbox\" (click)=\"selectconceptvalue(list.pk)\" > {{list.fields.name}}<br>\n      </span></div>\n    </div>\n      <div class=\"modal-footer\">\n        <button type=\"button\" class=\"btn btn-secondary\" (click)=\"myModal21.hide()\">{{'close'| translate }}</button>\n        <button type=\"button\" class=\"btn btn-primary\" (click)=\"getmultipleconceptids()\" style=background-color:#808080;color:white;>{{'ok'| translate }}</button>\n      </div>\n    </div><!-- /.modal-content -->\n  </div><!-- /.modal-dialog -->\n</div><!-- /.modal -->\n<!--end of multiple text-->\n\n\n<!--select terms-->\n<button id=\"terms\"  hidden type=\"button\" class=\"btn btn-secondary\" data-toggle=\"modal\" (click)=\"myModal14.show()\">\n     Launch demo modal\n</button>\n<div bsModal #myModal14=\"bs-modal\" class=\"modal fade\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\" aria-hidden=\"true\">\n  <div class=\"modal-dialog\" role=\"document\">\n    <div class=\"modal-content\">\n      <div class=\"modal-header\">\n          <h6 class=\"modal-title\" id=\"exampleModalLabel\" style=font-size:15px;>{{'select-terms'| translate }}</h6>\n        <button type=\"button\" class=\"close\" (click)=\"myModal14.hide()\" aria-label=\"Close\">\n          <span aria-hidden=\"true\">&times;</span>\n        </button>\n      </div>\n      <div class=\"modal-body\">\n      <div id=\"termsvalue\"> <span *ngFor=\"let color of finaltermlist\"> \n   \n    <input type=\"checkbox\" (click)=\"selecttermsvalue(color)\" > {{color}}<br>\n      </span></div>\n    </div>\n      <div class=\"modal-footer\">\n        <button type=\"button\" class=\"btn btn-secondary\" (click)=\"myModal14.hide()\">{{'close'| translate }}</button>\n        <button type=\"button\" class=\"btn btn-primary\" (click)=\"insertTerms()\" style=background-color:#808080;color:white;>{{'ok'| translate }}</button>\n      </div>\n    </div><!-- /.modal-content -->\n  </div><!-- /.modal-dialog -->\n</div><!-- /.modal -->\n\n\n<!--DELETE CONCEPT LIST Url-->\n <button id=\"delete-text-listurl\"  hidden type=\"button\" class=\"btn btn-secondary\" data-toggle=\"modal\" (click)=\"deleteUrlModal.show()\">\n            deleteTextListUrl\n </button>\n<div bsModal #deleteUrlModal=\"bs-modal\" class=\"modal fade\" tabindex=\"-1\" role=\"dialog\" aria-labelledby = \"myModalLabel\" aria-hidden=\"true\">\n  <div class=\"modal-dialog\" role=\"document\">\n    \n    <div class=\"modal-content\">\n      <div class=\"modal-header\">\n        <h4 class=\"modal-title\" style=font-size:15px;>Delete URL</h4>\n        <button type=\"button\" class=\"close\" (click)=\"deleteUrlModal.hide()\" aria-label=\"Close\">\n          <span aria-hidden=\"true\">&times;</span>\n        </button> \n      </div>  \n      <div class=\"modal-body\">\n        Do you really want to delete this URL....?\n             <input type=\"hidden\" id=\"delete-text-list-url\">\n           \n    </div>\n      <div class=\"modal-footer\">\n        <button type=\"button\" class=\"btn btn-secondary\" (click)=\"deleteUrlModal.hide()\">Close</button>\n        <button  style=\"background-color:#808080; color:white;;\" type=\"button\" class=\"btn btn-primary\" (click)=\"deleteUrl()\">Ok</button>\n      </div>\n    </div>\n    <!-- /.modal-content -->\n  </div><!-- /.modal-dialog -->\n</div><!-- /.modal --> \n                        \n\n<button id=\"defination\"  hidden type=\"button\" class=\"btn btn-secondary\" data-toggle=\"modal\" (click)=\"myModal16.show()\">\n      Launch demo modal\n</button>\n<div bsModal #myModal16=\"bs-modal\" class=\"modal fade\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\" aria-hidden=\"true\">\n  <div class=\"modal-dialog\" role=\"document\">\n    <div class=\"modal-content\">\n      <div class=\"modal-header\">\n          <h6 class=\"modal-title\" id=\"exampleModalLabel\" style=font-size:15px;>{{'addefination'| translate}}</h6>\n        <button type=\"button\" class=\"close\" (click)=\"myModal16.hide()\" aria-label=\"Close\">\n          <span aria-hidden=\"true\">&times;</span>\n        </button>\n      </div>\n      <div class=\"modal-body\" id=\"definationdiv\">\n     \n   \n    <textarea rows=4 cols=56 #defination id=\"def\"> </textarea>\n     \n    </div>\n      <div class=\"modal-footer\">\n        <button type=\"button\" class=\"btn btn-secondary\" (click)=\"myModal16.hide()\">{{'close'| translate }}</button>\n        <button type=\"button\" class=\"btn btn-primary\" (click)=\"saveDef(defination.value)\" style=background-color:#808080;color:white;>{{'Save'| translate }}</button>\n      </div>\n    </div><!-- /.modal-content -->\n  </div><!-- /.modal-dialog -->\n</div><!-- /.modal -->\n\n\n<!--RENAME FOLDER-->\n <button id=\"renameFolder\"  hidden type=\"button\" class=\"btn btn-secondary\" data-toggle=\"modal\" (click)=\"myModal1.show()\">\n            REname Folder\n          </button>\n<div bsModal #myModal1=\"bs-modal\" class=\"modal fade\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\" aria-hidden=\"true\">\n  <div class=\"modal-dialog\" role=\"document\">\n    <div class=\"modal-content\">\n      <div class=\"modal-header\">\n        <h4 class=\"modal-title\" style=font-size:15px;>{{'rename-folder'| translate }}</h4>\n        <button type=\"button\" class=\"close\" (click)=\"myModal1.hide()\" aria-label=\"Close\">\n          <span aria-hidden=\"true\">&times;</span>\n        </button>\n      </div>  \n      <div class=\"modal-body\">\n          <input type=\"text\" class=\"form-control\" id=\"folderRenameText\">  \n          <input type=\"text\" hidden class=\"form-control\" id=\"folderRenameId\"> \n    </div>\n      <div class=\"modal-footer\">\n        <button type=\"button\" class=\"btn btn-secondary\" (click)=\"myModal1.hide()\">{{'close'| translate }}</button>\n        <button type=\"button\" class=\"btn btn-primary\" (click)=\"renameFolder()\" style=background-color:#808080;color:white;>{{'Save'| translate }}</button>\n      </div>\n    </div><!-- /.modal-content -->\n  </div><!-- /.modal-dialog -->\n</div><!-- /.modal -->\n<!--select terms-->\n\n<!--RENAME FOLDER-->\n <button id=\"renameConceptFolder\"  hidden type=\"button\" class=\"btn btn-secondary\" data-toggle=\"modal\" (click)=\"mylastModal1.show()\">\n            REname Folder\n</button>\n<div bsModal #mylastModal1=\"bs-modal\" class=\"modal fade\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\" aria-hidden=\"true\">\n  <div class=\"modal-dialog\" role=\"document\">\n    <div class=\"modal-content\">\n      <div class=\"modal-header\">\n        <h4 class=\"modal-title\" style=font-size:15px;>{{'rename-folder'| translate }}</h4>\n        <button type=\"button\" class=\"close\" (click)=\"mylastModal1.hide()\" aria-label=\"Close\">\n          <span aria-hidden=\"true\">&times;</span>\n        </button>\n      </div>  \n      <div class=\"modal-body\">\n          <input type=\"text\" class=\"form-control\" id=\"folderRenameConceptText\">  \n          <input type=\"text\" hidden class=\"form-control\" id=\"folderRenameConceptId\"> \n    </div>\n      <div class=\"modal-footer\">\n        <button type=\"button\" class=\"btn btn-secondary\" (click)=\"mylastModal1.hide()\">{{'close'| translate }}</button>\n        <button type=\"button\" class=\"btn btn-primary\" (click)=\"renameFolderConcept()\" style=background-color:#808080;color:white;>{{'Save'| translate }}</button>\n      </div>\n    </div><!-- /.modal-content -->\n  </div><!-- /.modal-dialog -->\n</div><!-- /.modal -->\n<!--select terms-->\n\n\n\n\n\n\n\n\n<!--DELETE FOLDER-->\n <button id=\"deleteFolder\"  hidden type=\"button\" class=\"btn btn-secondary\" data-toggle=\"modal\" (click)=\"myModal2.show()\">\n            delete\n          </button>\n<div bsModal #myModal2=\"bs-modal\" class=\"modal fade\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\" aria-hidden=\"true\">\n  <div class=\"modal-dialog\" role=\"document\">\n    <div class=\"modal-content\">\n      <div class=\"modal-header\">\n        <h4 class=\"modal-title\" style=font-size:15px;>{{'delete-folder'| translate }}</h4>\n        <button type=\"button\" class=\"close\" (click)=\"myModal2.hide()\" aria-label=\"Close\">\n          <span aria-hidden=\"true\">&times;</span>\n        </button>\n      </div>  \n      <div class=\"modal-body\">\n           {{'delete-folder-msg'| translate}}\n          <input type=\"text\" hidden class=\"form-control\" id=\"folderDeleteId\"> \n    </div>\n      <div class=\"modal-footer\">\n        <button type=\"button\" class=\"btn btn-secondary\" (click)=\"myModal2.hide()\">{{'close'| translate }}</button>\n        <button style=\"background-color:#808080;color:white;\" type=\"button\" class=\"btn btn-primary\" (click)=\"deleteFolder()\">{{'ok'| translate }}</button>\n      </div>\n    </div><!-- /.modal-content -->\n  </div><!-- /.modal-dialog -->\n</div><!-- /.modal -->\n\n\n<!--DELETE FOLDER-->\n <button id=\"deleteConceptFolder\"  hidden type=\"button\" class=\"btn btn-secondary\" data-toggle=\"modal\" (click)=\"myconceptModal2.show()\">\n            delete\n          </button>\n<div bsModal #myconceptModal2=\"bs-modal\" class=\"modal fade\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\" aria-hidden=\"true\">\n  <div class=\"modal-dialog\" role=\"document\">\n    <div class=\"modal-content\">\n      <div class=\"modal-header\">\n        <h4 class=\"modal-title\" style=font-size:15px;>{{'delete-folder'| translate }}</h4>\n        <button type=\"button\" class=\"close\" (click)=\"myconceptModal2.hide()\" aria-label=\"Close\">\n          <span aria-hidden=\"true\">&times;</span>\n        </button>\n      </div>  \n      <div class=\"modal-body\">\n           {{'delete-folder-msg'| translate}}\n          <input type=\"text\" hidden class=\"form-control\" id=\"folderDeleteConceptId\"> \n      </div>\n      <div class=\"modal-footer\">\n        <button type=\"button\" class=\"btn btn-secondary\" (click)=\"myconceptModal2.hide()\">{{'close'| translate }}</button>\n        <button style=\"background-color:#808080;color:white;\" type=\"button\" class=\"btn btn-primary\" (click)=\"deleteConceptFolder()\">{{'ok'| translate }}</button>\n      </div>\n    </div><!-- /.modal-content -->\n  </div><!-- /.modal-dialog -->\n</div><!-- /.modal -->\n\n\n\n\n<!--ADD TEXT LIST-->\n <button id=\"addTextList\"  hidden type=\"button\" class=\"btn btn-secondary\" data-toggle=\"modal\" (click)=\"myModal3.show()\">\n            newTextList\n          </button>\n<div bsModal #myModal3=\"bs-modal\" class=\"modal fade\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\" aria-hidden=\"true\">\n  <div class=\"modal-dialog\" role=\"document\">\n    <div class=\"modal-content\">\n      <div class=\"modal-header\">\n        <h4 class=\"modal-title\" style=font-size:15px;>{{'New-Text-List'| translate }}</h4>\n        <button type=\"button\" class=\"close\" (click)=\"myModal3.hide()\" aria-label=\"Close\">\n          <span aria-hidden=\"true\">&times;</span>\n        </button> \n      </div>  \n      <div class=\"modal-body\">\n            \n          <input type=\"file\" (change)=\"fileChangeEvent($event)\" multiple name=\"text-list-new\" class=\"form-control\" id=\"newTextListFile\"> \n          <input type=\"hidden\" id=\"id-hidden-for-new-text-list\">\n    </div>\n      <div class=\"modal-footer\">\n        <button type=\"button\" class=\"btn btn-secondary\" (click)=\"myModal2.hide()\">{{'close'| translate }}</button>\n        <button   type=\"button\" class=\"btn btn-primary\" (click)=\"addTextList()\" style=background-color:#808080;color:white;>{{'ok'| translate }}</button>\n      </div>\n    </div><!-- /.modal-content -->\n  </div><!-- /.modal-dialog -->\n</div><!-- /.modal -->\n\n<div class={{loaderFlag}} ></div>\n\n<!--ADD CONCEPT LIST-->\n<button id=\"addConceptList\"  hidden type=\"button\" class=\"btn btn-secondary\" data-toggle=\"modal\" (click)=\"myModal4.show()\">\nnewconceptlist\n</button>\n<div bsModal #myModal4=\"bs-modal\" class=\"modal fade\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\" aria-hidden=\"true\">\n   <div class=\"modal-dialog\" role=\"document\">\n      <div class=\"modal-content\">\n         <div class=\"modal-header\">\n            <h4 class=\"modal-title\" style=font-size:15px;>{{'New-Concept-List'| translate }}</h4>\n            <button type=\"button\" class=\"close\" (click)=\"myModal4.hide()\" aria-label=\"Close\">\n            <span aria-hidden=\"true\">&times;</span>\n            </button> \n         </div>\n         <div class=\"modal-body\">\n            <input type=\"file\" (change)=\"fileChangeEvent2($event)\" name=\"text-list-new\" class=\"form-control\" id=\"newConceptListFile\"> \n         </div>\n         <div class=\"modal-footer\">\n            <button type=\"button\" class=\"btn btn-secondary\" (click)=\"myModal4.hide()\">{{'close'| translate }}</button>\n            <button   type=\"button\" class=\"btn btn-primary\" (click)=\"addConceptList()\" style=background-color:#808080;color:white;>{{'ok'| translate }}</button>\n         </div>\n      </div>\n      <!-- /.modal-content -->\n   </div>\n   <!-- /.modal-dialog -->\n</div>\n<!-- /.modal -->\n\n<!--ADD Stop LIST-->\n<div>\n   <button id=\"addConceptStopList\"  hidden type=\"button\" class=\"btn btn-secondary\" data-toggle=\"modal\" (click)=\"myModal11.show()\">\n   New Stop List\n   </button>\n   <div bsModal #myModal11=\"bs-modal\" class=\"modal fade\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\" aria-hidden=\"true\">\n      <div class=\"modal-dialog\" role=\"document\">\n         <div class=\"modal-content\">\n            <div class=\"modal-header\">\n               <h4 class=\"modal-title\" style=font-size:15px;>New Stop List</h4>\n               <button type=\"button\" class=\"close\" (click)=\"myModal11.hide()\" aria-label=\"Close\">\n               <span aria-hidden=\"true\">&times;</span>\n               </button> \n            </div>\n            <div class=\"modal-body\">\n               <input type=\"file\" (change)=\"fileChangeEventStopList($event)\" name=\"text-list-new\" class=\"form-control\" id=\"newStopListFile\"> \n            </div>\n            <div class=\"modal-footer\">\n               <button type=\"button\" class=\"btn btn-secondary\" (click)=\"myModal11.hide()\">{{'close'| translate }}</button>\n               <button   type=\"button\" class=\"btn btn-primary\" (click)=\"addStopList()\" style=background-color:#808080;color:white;>{{'ok'| translate }}</button>\n            </div>\n         </div>\n         <!-- /.modal-content -->\n      </div>\n      <!-- /.modal-dialog -->\n   </div>\n   <!-- /.modal -->\n</div>\n\n\n\n\n<!--DELETE CONCEPT LIST-->\n<button id=\"deleteConceptList\"  hidden type=\"button\" class=\"btn btn-secondary\" data-toggle=\"modal\" (click)=\"myModal5.show()\">\n      deleteconceptlist\n</button>\n<div bsModal #myModal5=\"bs-modal\" class=\"modal fade\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\" aria-hidden=\"true\">\n  <div class=\"modal-dialog\" role=\"document\">\n    <div class=\"modal-content\">\n      <div class=\"modal-header\">\n        <h4 class=\"modal-title\" style=font-size:15px;>{{'delete-concept'| translate }}</h4>\n        <button type=\"button\" class=\"close\" (click)=\"myModal5.hide()\" aria-label=\"Close\">\n          <span aria-hidden=\"true\">&times;</span>\n        </button> \n      </div>  \n      <div class=\"modal-body\">\n        {{'delete-concept-list'| translate }}\n             <input type=\"hidden\" id=\"delete-concept-list-hidden-pop\">\n           \n    </div>\n      <div class=\"modal-footer\">\n        <button type=\"button\" class=\"btn btn-secondary\" (click)=\"myModal5.hide()\">{{'close'| translate }}</button>\n        <button  style=\"background-color:#808080; color:white;;\" type=\"button\" class=\"btn btn-primary\" (click)=\"deleteeConceptList()\">{{'ok'| translate }}</button>\n      </div>\n    </div><!-- /.modal-content -->\n  </div><!-- /.modal-dialog -->\n</div><!-- /.modal -->\n\n\n<!--RENAME CONCEPT LIST-->\n<button id=\"renameConceptList\"  hidden type=\"button\" class=\"btn btn-secondary\" data-toggle=\"modal\" (click)=\"myModal6.show()\">\nrenameconceptlist\n</button>\n<div bsModal #myModal6=\"bs-modal\" class=\"modal fade\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\" aria-hidden=\"true\">\n   <div class=\"modal-dialog\" role=\"document\">\n      <div class=\"modal-content\">\n         <div class=\"modal-header\">\n            <h4 class=\"modal-title\"style=font-size:15px;>{{'Rename-Concept-List'| translate }}</h4>\n            <button type=\"button\" class=\"close\" (click)=\"myModal6.hide()\" aria-label=\"Close\">\n            <span aria-hidden=\"true\">&times;</span>\n            </button> \n         </div>\n         <div class=\"modal-body\">\n            <input type=\"hidden\" id=\"pop-up-concpt-id\" >\n            <input type=\"text\" id=\"pop-up-concept-name\"  class=\"form-control\"  >\n         </div>\n         <div class=\"modal-footer\">\n            <button type=\"button\" class=\"btn btn-secondary\" (click)=\"myModal6.hide()\">{{'close'| translate }}</button>\n            <button  style=\"background-color:#808080; color:white;\" type=\"button\" class=\"btn btn-primary\" (click)=\"renameeConceptList()\">{{'ok'| translate }}</button>\n         </div>\n      </div>\n      <!-- /.modal-content -->\n   </div>\n   <!-- /.modal-dialog -->\n</div>\n<!-- /.modal -->\n\n\n  <!--RENAME STOP  LIST-->\n    <button id=\"renameStopList\"  hidden type=\"button\" class=\"btn btn-secondary\" data-toggle=\"modal\" (click)=\"myModal12.show()\">\n    renamestoplist\n    </button>\n    <div bsModal #myModal12=\"bs-modal\" class=\"modal fade\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\" aria-hidden=\"true\">\n      <div class=\"modal-dialog\" role=\"document\">\n          <div class=\"modal-content\">\n            <div class=\"modal-header\">\n                <h4 class=\"modal-title\"style=font-size:15px;>Rename Stop List</h4>\n                <button type=\"button\" class=\"close\" (click)=\"myModal12.hide()\" aria-label=\"Close\">\n                <span aria-hidden=\"true\">&times;</span>\n                </button> \n            </div>\n            <div class=\"modal-body\">\n                <input type=\"hidden\" id=\"pop-up-stop-id\" >\n                <input type=\"text\" id=\"pop-up-stop-name\"  class=\"form-control\"  >\n            </div>\n            <div class=\"modal-footer\">\n                <button type=\"button\" class=\"btn btn-secondary\" (click)=\"myModal12.hide()\">{{'close'| translate }}</button>\n                <button  style=\"background-color:#808080; color:white;\" type=\"button\" class=\"btn btn-primary\" (click)=\"renameGivenStopList()\">{{'ok'| translate }}</button>\n            </div>\n          </div>\n          <!-- /.modal-content -->\n      </div>\n      <!-- /.modal-dialog -->\n    </div>\n    <!-- /.modal -->\n\n\n<!--DELETE Stop LIST-->\n<button id=\"deleteStopList\"  hidden type=\"button\" class=\"btn btn-secondary\" data-toggle=\"modal\" (click)=\"myModal13.show()\">\ndeletestoplist\n</button>\n<div bsModal #myModal13=\"bs-modal\" class=\"modal fade\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\" aria-hidden=\"true\">\n   <div class=\"modal-dialog\" role=\"document\">\n      <div class=\"modal-content\">\n         <div class=\"modal-header\">\n            <h4 class=\"modal-title\" style=font-size:15px;>Delete Stop List</h4>\n            <button type=\"button\" class=\"close\" (click)=\"myModal13.hide()\" aria-label=\"Close\">\n            <span aria-hidden=\"true\">&times;</span>\n            </button> \n         </div>\n         <div class=\"modal-body\">\n            Do you really want to delete this Stop list....?\n            <input type=\"hidden\" id=\"delete-stop-list-hidden-pop\">\n         </div>\n         <div class=\"modal-footer\">\n            <button type=\"button\" class=\"btn btn-secondary\" (click)=\"myModal13.hide()\">{{'close'| translate }}</button>\n            <button  style=\"background-color:#808080; color:white;;\" type=\"button\" class=\"btn btn-primary\" (click)=\"deletedStopList()\">{{'ok'| translate }}</button>\n         </div>\n      </div>\n      <!-- /.modal-content -->\n   </div>\n   <!-- /.modal-dialog -->\n</div>\n<!-- /.modal -->\n\n  <aside class=\"aside-menu\">\n    <tabset>\n      <tab>\n        <template tabHeading><i class=\"icon-list\"></i></template>\n        <div class=\"callout m-0 py-h text-muted text-center bg-faded text-uppercase\">\n          <small><b>Today</b></small>\n        </div>\n        <hr class=\"transparent mx-1 my-0\">\n        <div class=\"callout callout-warning m-0 py-1\">\n          <div class=\"avatar float-right\">\n            <img src=\"assets/img/avatars/7.jpg\" class=\"img-avatar\" alt=\"admin@bootstrapmaster.com\">\n          </div>\n          <div>Meeting with <strong>Lucas</strong></div>\n          <small class=\"text-muted mr-1\"><i class=\"icon-calendar\"></i>&nbsp; 1 - 3pm</small>\n          <small class=\"text-muted\"><i class=\"icon-location-pin\"></i>&nbsp; Palo Alto, CA </small>\n        </div>\n        <hr class=\"mx-1 my-0\">\n        <div class=\"callout callout-info m-0 py-1\">\n          <div class=\"avatar float-right\">\n            <img src=\"assets/img/avatars/4.jpg\" class=\"img-avatar\" alt=\"admin@bootstrapmaster.com\">\n          </div>\n          <div>Skype with <strong>Megan</strong></div>\n          <small class=\"text-muted mr-1\"><i class=\"icon-calendar\"></i>&nbsp; 4 - 5pm</small>\n          <small class=\"text-muted\"><i class=\"icon-social-skype\"></i>&nbsp; On-line </small>\n        </div>\n        <hr class=\"transparent mx-1 my-0\">\n        <div class=\"callout m-0 py-h text-muted text-center bg-faded text-uppercase\">\n          <small><b>Tomorrow</b></small>\n        </div>\n        <hr class=\"transparent mx-1 my-0\">\n        <div class=\"callout callout-danger m-0 py-1\">\n          <div>New UI Project - <strong>deadline</strong></div>\n          <small class=\"text-muted mr-1\"><i class=\"icon-calendar\"></i>&nbsp; 10 - 11pm</small>\n          <small class=\"text-muted\"><i class=\"icon-home\"></i>&nbsp; Repindex </small>\n          <div class=\"avatars-stack mt-h\">\n            <div class=\"avatar avatar-xs\">\n              <img src=\"assets/img/avatars/2.jpg\" class=\"img-avatar\" alt=\"admin@bootstrapmaster.com\">\n            </div>\n            <div class=\"avatar avatar-xs\">\n              <img src=\"assets/img/avatars/3.jpg\" class=\"img-avatar\" alt=\"admin@bootstrapmaster.com\">\n            </div>\n            <div class=\"avatar avatar-xs\">\n              <img src=\"assets/img/avatars/4.jpg\" class=\"img-avatar\" alt=\"admin@bootstrapmaster.com\">\n            </div>\n            <div class=\"avatar avatar-xs\">\n              <img src=\"assets/img/avatars/5.jpg\" class=\"img-avatar\" alt=\"admin@bootstrapmaster.com\">\n            </div>\n            <div class=\"avatar avatar-xs\">\n              <img src=\"assets/img/avatars/6.jpg\" class=\"img-avatar\" alt=\"admin@bootstrapmaster.com\">\n            </div>\n          </div>\n        </div>\n        <hr class=\"mx-1 my-0\">\n        <div class=\"callout callout-success m-0 py-1\">\n          <div><strong>#10 Startups.Garden</strong> Meetup</div>\n          <small class=\"text-muted mr-1\"><i class=\"icon-calendar\"></i>&nbsp; 1 - 3pm</small>\n          <small class=\"text-muted\"><i class=\"icon-location-pin\"></i>&nbsp; Palo Alto, CA </small>\n        </div>\n        <hr class=\"mx-1 my-0\">\n        <div class=\"callout callout-primary m-0 py-1\">\n          <div><strong>Team meeting</strong></div>\n          <small class=\"text-muted mr-1\"><i class=\"icon-calendar\"></i>&nbsp; 4 - 6pm</small>\n          <small class=\"text-muted\" style=\"font-size:12px;\"><i class=\"icon-home\"></i>&nbsp; Copyright © 2018 Repindex Ltd. All rights reserved.</small>\n          <div class=\"avatars-stack mt-h\">\n            <div class=\"avatar avatar-xs\">\n              <img src=\"assets/img/avatars/2.jpg\" class=\"img-avatar\" alt=\"admin@bootstrapmaster.com\">\n            </div>\n            <div class=\"avatar avatar-xs\">\n              <img src=\"assets/img/avatars/3.jpg\" class=\"img-avatar\" alt=\"admin@bootstrapmaster.com\">\n            </div>\n            <div class=\"avatar avatar-xs\">\n              <img src=\"assets/img/avatars/4.jpg\" class=\"img-avatar\" alt=\"admin@bootstrapmaster.com\">\n            </div>\n            <div class=\"avatar avatar-xs\">\n              <img src=\"assets/img/avatars/5.jpg\" class=\"img-avatar\" alt=\"admin@bootstrapmaster.com\">\n            </div>\n            <div class=\"avatar avatar-xs\">\n              <img src=\"assets/img/avatars/6.jpg\" class=\"img-avatar\" alt=\"admin@bootstrapmaster.com\">\n            </div>\n            <div class=\"avatar avatar-xs\">\n              <img src=\"assets/img/avatars/7.jpg\" class=\"img-avatar\" alt=\"admin@bootstrapmaster.com\">\n            </div>\n            <div class=\"avatar avatar-xs\">\n              <img src=\"assets/img/avatars/8.jpg\" class=\"img-avatar\" alt=\"admin@bootstrapmaster.com\">\n            </div>\n          </div>\n        </div>\n        <hr class=\"mx-1 my-0\">\n      </tab>\n      <tab>\n        <template tabHeading><i class=\"icon-speech\"></i></template>\n        <div class=\"p-1\">\n          <div class=\"message\">\n            <div class=\"py-1 pb-3 mr-1 float-left\">\n              <div class=\"avatar\">\n                <img src=\"assets/img/avatars/7.jpg\" class=\"img-avatar\" alt=\"admin@bootstrapmaster.com\">\n                <span class=\"avatar-status badge-success\"></span>\n              </div>\n            </div>\n            <div>\n              <small class=\"text-muted\">Lukasz Holeczek</small>\n              <small class=\"text-muted float-right mt-q\">1:52 PM</small>\n            </div>\n            <div class=\"text-truncate font-weight-bold\">Lorem ipsum dolor sit amet</div>\n            <small class=\"text-muted\">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt...</small>\n          </div>\n          <hr>\n          <div class=\"message\">\n            <div class=\"py-1 pb-3 mr-1 float-left\">\n              <div class=\"avatar\">\n                <img src=\"assets/img/avatars/7.jpg\" class=\"img-avatar\" alt=\"admin@bootstrapmaster.com\">\n                <span class=\"avatar-status badge-success\"></span>\n              </div>\n            </div>\n            <div>\n              <small class=\"text-muted\">Lukasz Holeczek</small>\n              <small class=\"text-muted float-right mt-q\">1:52 PM</small>\n            </div>\n            <div class=\"text-truncate font-weight-bold\">Lorem ipsum dolor sit amet</div>\n            <small class=\"text-muted\">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt...</small>\n          </div>\n          <hr>\n          <div class=\"message\">\n            <div class=\"py-1 pb-3 mr-1 float-left\">\n              <div class=\"avatar\">\n                <img src=\"assets/img/avatars/7.jpg\" class=\"img-avatar\" alt=\"admin@bootstrapmaster.com\">\n                <span class=\"avatar-status badge-success\"></span>\n              </div>\n            </div>\n            <div>\n              <small class=\"text-muted\">Lukasz Holeczek</small>\n              <small class=\"text-muted float-right mt-q\">1:52 PM</small>\n            </div>\n            <div class=\"text-truncate font-weight-bold\">Lorem ipsum dolor sit amet</div>\n            <small class=\"text-muted\">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt...</small>\n          </div>\n          <hr>\n          <div class=\"message\">\n            <div class=\"py-1 pb-3 mr-1 float-left\">\n              <div class=\"avatar\">\n                <img src=\"assets/img/avatars/7.jpg\" class=\"img-avatar\" alt=\"admin@bootstrapmaster.com\">\n                <span class=\"avatar-status badge-success\"></span>\n              </div>\n            </div>\n            <div>\n              <small class=\"text-muted\">Lukasz Holeczek</small>\n              <small class=\"text-muted float-right mt-q\">1:52 PM</small>\n            </div>\n            <div class=\"text-truncate font-weight-bold\">Lorem ipsum dolor sit amet</div>\n            <small class=\"text-muted\">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt...</small>\n          </div>\n          <hr>\n          <div class=\"message\">\n            <div class=\"py-1 pb-3 mr-1 float-left\">\n              <div class=\"avatar\">\n                <img src=\"assets/img/avatars/7.jpg\" class=\"img-avatar\" alt=\"admin@bootstrapmaster.com\">\n                <span class=\"avatar-status badge-success\"></span>\n              </div>\n            </div>\n            <div>\n              <small class=\"text-muted\">Lukasz Holeczek</small>\n              <small class=\"text-muted float-right mt-q\">1:52 PM</small>\n            </div>\n            <div class=\"text-truncate font-weight-bold\">Lorem ipsum dolor sit amet</div>\n            <small class=\"text-muted\">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt...</small>\n          </div>\n        </div>\n      </tab>\n      <tab>\n        <template tabHeading><i class=\"icon-settings\"></i></template>\n        <div class=\"p-1\">\n          <h6>Settings</h6>\n\n          <div class=\"aside-options\">\n            <div class=\"clearfix mt-2\">\n              <small><b>Option 1</b></small>\n              <label class=\"switch switch-text switch-pill switch-success switch-sm float-right\">\n                <input type=\"checkbox\" class=\"switch-input\" checked>\n                <span class=\"switch-label\" data-on=\"On\" data-off=\"Off\"></span>\n                <span class=\"switch-handle\"></span>\n              </label>\n            </div>\n            <div>\n              <small class=\"text-muted\">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</small>\n            </div>\n          </div>\n\n          <div class=\"aside-options\">\n            <div class=\"clearfix mt-1\">\n              <small><b>Option 2</b></small>\n              <label class=\"switch switch-text switch-pill switch-success switch-sm float-right\">\n                <input type=\"checkbox\" class=\"switch-input\">\n                <span class=\"switch-label\" data-on=\"On\" data-off=\"Off\"></span>\n                <span class=\"switch-handle\"></span>\n              </label>\n            </div>\n            <div>\n              <small class=\"text-muted\">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</small>\n            </div>\n          </div>\n\n          <div class=\"aside-options\">\n            <div class=\"clearfix mt-1\">\n              <small><b>Option 3</b></small>\n              <label class=\"switch switch-text switch-pill switch-success switch-sm float-right\">\n                <input type=\"checkbox\" class=\"switch-input\">\n                <span class=\"switch-label\" data-on=\"On\" data-off=\"Off\"></span>\n                <span class=\"switch-handle\"></span>\n              </label>\n            </div>\n          </div>\n\n          <div class=\"aside-options\">\n            <div class=\"clearfix mt-1\">\n              <small><b>Option 4</b></small>\n              <label class=\"switch switch-text switch-pill switch-success switch-sm float-right\">\n                <input type=\"checkbox\" class=\"switch-input\" checked>\n                <span class=\"switch-label\" data-on=\"On\" data-off=\"Off\"></span>\n                <span class=\"switch-handle\"></span>\n              </label>\n            </div>\n          </div>\n\n          <hr>\n          <h6>System Utilization</h6>\n\n          <div class=\"text-uppercase mb-q mt-2\"><small><b>CPU Usage</b></small></div>\n          <div class=\"progress progress-xs\">\n            <div class=\"progress-bar bg-info\" role=\"progressbar\" style=\"width: 25%\" aria-valuenow=\"25\" aria-valuemin=\"0\" aria-valuemax=\"100\"></div>\n          </div>\n          <small class=\"text-muted\">348 Processes. 1/4 Cores.</small>\n\n          <div class=\"text-uppercase mb-q mt-h\"><small><b>Memory Usage</b></small></div>\n          <div class=\"progress progress-xs\">\n            <div class=\"progress-bar bg-warning\" role=\"progressbar\" style=\"width: 70%\" aria-valuenow=\"70\" aria-valuemin=\"0\" aria-valuemax=\"100\"></div>\n          </div>\n          <small class=\"text-muted\">11444GB/16384MB</small>\n\n          <div class=\"text-uppercase mb-q mt-h\"><small><b>SSD 1 Usage</b></small></div>\n          <div class=\"progress progress-xs\">\n            <div class=\"progress-bar bg-danger\" role=\"progressbar\" style=\"width: 95%\" aria-valuenow=\"95\" aria-valuemin=\"0\" aria-valuemax=\"100\"></div>\n          </div>\n          <small class=\"text-muted\">243GB/256GB</small>\n\n          <div class=\"text-uppercase mb-q mt-h\"><small><b>SSD 2 Usage</b></small></div>\n          <div class=\"progress progress-xs\">\n            <div class=\"progress-bar bg-success\" role=\"progressbar\" style=\"width: 10%\" aria-valuenow=\"10\" aria-valuemin=\"0\" aria-valuemax=\"100\"></div>\n          </div>\n          <small class=\"text-muted\">25GB/256GB</small>\n        </div>\n      </tab>\n    </tabset>\n  </aside>\n</div>\n<!--DELETE CONCEPT LIST-->\n <button id=\"deleteTextList\"  hidden type=\"button\" class=\"btn btn-secondary\" data-toggle=\"modal\" (click)=\"myModal66.show()\">\n            deleteTextList\n          </button>\n<div bsModal #myModal66=\"bs-modal\" class=\"modal fade\" tabindex=\"-1\" role=\"dialog\" aria-labelledby = \"myModalLabel\" aria-hidden=\"true\">\n  <div class=\"modal-dialog\" role=\"document\">\n    \n    <div class=\"modal-content\">\n      <div class=\"modal-header\">\n        <h4 class=\"modal-title\" style=font-size:15px;>Delete Text List</h4>\n        <button type=\"button\" class=\"close\" (click)=\"myModal66.hide()\" aria-label=\"Close\">\n          <span aria-hidden=\"true\">&times;</span>\n        </button> \n      </div>  \n      <div class=\"modal-body\">\n        Do you really want to delete this Text list....?\n             <input type=\"hidden\" id=\"delete-text-list-hidden-pop\">\n           \n    </div>\n      <div class=\"modal-footer\">\n        <button type=\"button\" class=\"btn btn-secondary\" (click)=\"myModal66.hide()\">Close</button>\n        <button  style=\"background-color:#808080; color:white;;\" type=\"button\" class=\"btn btn-primary\" (click)=\"deleteeTextList()\">Ok</button>\n      </div>\n    </div>\n    <!-- /.modal-content -->\n  </div><!-- /.modal-dialog -->\n</div><!-- /.modal --> \n\n<div>\n<button id=\"addMasterList\"  hidden type=\"button\" class=\"btn btn-secondary\" data-toggle=\"modal\" (click)=\"myModal51.show()\">\n   New Master List\n   </button>\n   <div bsModal #myModal51=\"bs-modal\" class=\"modal fade\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\" aria-hidden=\"true\">\n      <div class=\"modal-dialog\" role=\"document\">\n         <div class=\"modal-content\">\n            <div class=\"modal-header\">\n               <h4 class=\"modal-title\" style=font-size:15px;>New Master List</h4>\n               <button type=\"button\" class=\"close\" (click)=\"myModal51.hide()\" aria-label=\"Close\">\n               <span aria-hidden=\"true\">&times;</span>\n               </button> \n            </div>\n            <div class=\"modal-body\">\n               <input type=\"file\" (change)=\"fileChangeEventMasterList($event)\" name=\"text-list-new\" class=\"form-control\" id=\"newMasterListFile\"> \n            </div>\n            <div class=\"modal-footer\">\n               <button type=\"button\" class=\"btn btn-secondary\" (click)=\"myModal51.hide()\">Close</button>\n               <button   type=\"button\" class=\"btn btn-primary\" (click)=\"addMasterList()\" style=background-color:#808080;color:white;>Ok</button>\n            </div>\n         </div>\n         <!-- /.modal-content -->\n      </div>\n      <!-- /.modal-dialog -->\n   </div>\n   <!-- /.modal -->\n</div>\n\n <button id=\"myModalMaster\"  hidden type=\"button\" class=\"btn btn-secondary\" data-toggle=\"modal\" (click)=\"myModalMaster.show()\">\n    renamemasterlist\n    </button>\n    <div bsModal #myModalMaster=\"bs-modal\" class=\"modal fade\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\" aria-hidden=\"true\">\n      <div class=\"modal-dialog\" role=\"document\">\n          <div class=\"modal-content\">\n            <div class=\"modal-header\">\n                <h4 class=\"modal-title\"style=font-size:15px;>Rename Master List</h4>\n                <button type=\"button\" class=\"close\" (click)=\"myModalMaster.hide()\" aria-label=\"Close\">\n                <span aria-hidden=\"true\">&times;</span>\n                </button> \n            </div>\n            <div class=\"modal-body\">\n                <input type=\"hidden\" id=\"pop-up-master-id\" >\n                <input type=\"text\" id=\"pop-up-master-name\"  class=\"form-control\"  >\n            </div>\n            <div class=\"modal-footer\">\n                <button type=\"button\" class=\"btn btn-secondary\" (click)=\"myModalMaster.hide()\">Close</button>\n                <button  style=\"background-color:#808080; color:white;\" type=\"button\" class=\"btn btn-primary\" (click)=\"renameGivenMasterList()\">Ok</button>\n            </div>\n          </div>\n          <!-- /.modal-content -->\n      </div>\n      <!-- /.modal-dialog -->\n    </div>\n    <!-- /.modal -->\n    <button id=\"subfolder\"  hidden type=\"button\" class=\"btn btn-secondary\" data-toggle=\"modal\" (click)=\"myModal27.show()\">\n            newTextList\n          </button>\n<div bsModal #myModal27=\"bs-modal\" class=\"modal fade\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\" aria-hidden=\"true\">\n  <div class=\"modal-dialog\" role=\"document\">\n    <div class=\"modal-content\">\n      <div class=\"modal-header\">\n        <h4 class=\"modal-title\" style=font-size:15px;>Add Newfolder</h4>\n        <button type=\"button\" class=\"close\" (click)=\"myModal27.hide()\" aria-label=\"Close\">\n          <span aria-hidden=\"true\">&times;</span>\n        </button> \n      </div>  \n      <div class=\"modal-body\">\n            \n          <input type=\"text\"  name=\"sub\" class=\"form-control\" id=\"sub\"> \n          <input type=\"hidden\" id=\"hiddenfolderId\">\n          \n    </div>\n      <div class=\"modal-footer\">\n        <button type=\"button\" class=\"btn btn-secondary\" (click)=\"myModal27.hide()\">{{'close'| translate }}</button>\n        <button   type=\"button\" class=\"btn btn-primary\" (click)=\"addsub()\" style=background-color:#808080;color:white;>{{'ok'| translate }}</button>\n      </div>\n    </div><!-- /.modal-content -->\n  </div><!-- /.modal-dialog -->\n</div><!-- /.modal -->\n\n<button id=\"addurl\"  hidden type=\"button\" class=\"btn btn-secondary\" data-toggle=\"modal\" (click)=\"urlModal.show()\">\n            newTextList\n</button>\n<div bsModal #urlModal=\"bs-modal\" class=\"modal fade\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\" aria-hidden=\"true\">\n  <div class=\"modal-dialog\" role=\"document\">\n    <div class=\"modal-content\">\n      <div class=\"modal-header\">\n          <h4 class=\"modal-title\" style=font-size:15px;>Add URL</h4>\n          <button type=\"button\" class=\"close\" (click)=\"urlModal.hide()\" aria-label=\"Close\">\n            <span aria-hidden=\"true\">&times;</span>\n          </button> \n      </div>  \n      <div class=\"modal-body\">\n          <input type=\"text\"  name=\"name\" class=\"form-control\" id=\"urlname\" placeholder=\"URL Name\"> <br>            \n          <input type=\"text\"  name=\"url\" class=\"form-control\" id=\"url\" placeholder=\"URL\"> \n          <input type=\"hidden\" id=\"hiddenurlId\">          \n      </div>\n      <div class=\"modal-footer\">\n        <button type=\"button\" class=\"btn btn-secondary\" (click)=\"urlModal.hide()\">{{'close'| translate }}</button>\n        <button   type=\"button\" class=\"btn btn-primary\" (click)=\"addUrl()\" style=background-color:#808080;color:white;>{{'ok'| translate }}</button>\n      </div>\n    </div><!-- /.modal-content -->\n  </div><!-- /.modal-dialog -->\n</div><!-- /.modal -->\n\n\n <!--DELETE Master LIST-->\n<button id=\"deleteMasterList\"  hidden type=\"button\" class=\"btn btn-secondary\" data-toggle=\"modal\" (click)=\"myModal101.show()\">\n            deleteMasterList \n</button>\n<div bsModal #myModal101=\"bs-modal\" class=\"modal fade\" tabindex=\"-1\" role=\"dialog\" aria-labelledby = \"myModalLabel\" aria-hidden=\"true\">\n  <div class=\"modal-dialog\" role=\"document\">\n    \n    <div class=\"modal-content\">\n      <div class=\"modal-header\">\n        <h4 class=\"modal-title\" style=font-size:15px;>Delete Master List</h4>\n        <button type=\"button\" class=\"close\" (click)=\"myModal101.hide()\" aria-label=\"Close\">\n          <span aria-hidden=\"true\">&times;</span>\n        </button> \n      </div>  \n      <div class=\"modal-body\">\n        Do you really want to delete this Master concept  list....?\n             <input type=\"hidden\" id=\"delete-master-list-hidden-pop\">\n           \n    </div>\n      <div class=\"modal-footer\">\n        <button type=\"button\" class=\"btn btn-secondary\" (click)=\"myModal101.hide()\">Close</button>\n        <button  style=\"background-color:#808080; color:white;;\" type=\"button\" class=\"btn btn-primary\" (click)=\"deleteeMasterList()\">Ok</button>\n      </div>\n    </div>\n    <!-- /.modal-content -->\n  </div><!-- /.modal-dialog -->\n</div><!-- /.modal --> \n\n<!--concept not present-->\n<button id=\"cancelModalConcept\"  hidden type=\"button\" class=\"btn btn-secondary\" data-toggle=\"modal\" (click)=\"myModal1055.show()\">\n            cancelModalConcept \n</button>\n<div bsModal #myModal1055=\"bs-modal\" class=\"modal fade\" tabindex=\"-1\" role=\"dialog\" aria-labelledby = \"myModalLabel\" aria-hidden=\"true\">\n  <div class=\"modal-dialog\" role=\"document\">\n    \n    <div class=\"modal-content\">\n      <div class=\"modal-header\">\n        <h4 class=\"modal-title\" style=font-size:16px;>Alert</h4>\n        <button type=\"button\" class=\"close\" (click)=\"myModal1055.hide()\" aria-label=\"Close\">\n          <span aria-hidden=\"true\">&times;</span>\n        </button> \n      </div>  \n      <div class=\"modal-body\">\n        Concept that you searching not present in Knowledge .\n        Please try with other concept.\n             <input type=\"hidden\" id=\"delete-master-list-hidden-pop\">\n           \n    </div>\n      <div class=\"modal-footer\">\n        <button type=\"button\" class=\"btn btn-secondary\" (click)=\"myModal1055.hide()\">Close</button>\n        \n      </div>\n    </div>\n    <!-- /.modal-content -->\n  </div><!-- /.modal-dialog -->\n</div><!-- /.modal --> \n<!--end-->\n\n\n<footer class=\"app-footer\" style=\"font-size:12px;\">\n  <a href=\"http://coreui.io\"></a> © Mattersmith Limited 2018.\n   <span class=\"float-right\">Powered By Repindex © <b style=\"color:orange\">Repindex</b> Limited 2018</span>\n</footer>\n\n\n\n\n"

/***/ }),

/***/ 740:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(394);


/***/ })

},[740]);
//# sourceMappingURL=main.bundle.js.map