# Running Migrations

Applying a migration is done by executing the appropriate cypher script against neo4j

## Backup 

Ensure a backup is made beforehand using 

    $ ./neo-dump.sh

and

    $ http -j GET $API_URL/api/export/_graph > dumps/$BACKUP_FILENAME.json

## Migration

apply the migration with:

    $ cat 0001_fix_keywords.cypher | docker exec -i neo4j2 bin/neo4j-shell

or just 

    $ neo4j-shell < migration_script.cypher

**Warning**: Migration scripts /should/ be idempotent (capable of being applied
more than once without consequence), but it is still strongly advised to keep
track of which migrations have been applied to a given database instance, and
avoid re-running already-applied migrations.

## Reverting / rolling back

If a migration should fail, the neo4j database should be restored from backup using
either the app-level json backup (from above):

    $ http --timeout 500 --auth $CREDS -j POST $API_URL/api/import/_graph < $BACKUP_FILENAME.json
    
or from the neo4j cypher backup produced by the neo-dump script by:

    $ neo4j-shell -c 'match (x) detach delete x;'
    $ neo4j-shell < $BACKUP_FILENAME.cypher
    
