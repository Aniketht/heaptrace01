MATCH (x:Topic:Instance) WHERE exists(x.keywords) AND not(exists(x._tmp_kwlist))
 SET x._tmp_kwlist = [kw2 in reduce(ret = [], r in [kw in x.keywords | split(kw, ',')] | ret + r) | trim(kw2)];


MATCH (x:Topic:Instance) WHERE exists(x._tmp_kwlist)
 SET x.keywords = x._tmp_kwlist;

MATCH (x:Topic:Instance) WHERE exists(x._tmp_kwlist)
 REMOVE x._tmp_kwlist;
