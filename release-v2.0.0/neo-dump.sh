#!/bin/bash
set -euo pipefail
if [[ -z "$1" ]]; then
    echo 'container name required' >&2
    echo 'Available containers:'
    docker ps -f ancestor=neo4j:3.2.7 --format '{{ .Names }}' | nl
    exit 1
fi

NEO4J_CONTAINER=$1
if [[ -z ${2:-} ]]; then
    if [[ ! -d "./dumps/" ]]; then
        mkdir dumps
    fi
    DUMP_DEST="dumps/${NEO4J_CONTAINER}-$(date -Iseconds)-export.cypher"
else
    DUMP_DEST=$2
fi

docker exec $NEO4J_CONTAINER bin/neo4j-shell -c dump > $DUMP_DEST
echo "dumped to $DUMP_DEST" >&2
