# Initial Data for neo4j bootstrapping

The `graphclean.json` file in this directory can be loaded into an empty neo4j instance to populate the
initial objects required by the application. Details for how to load it (via the webapi http endpoint)
can be found in the root README of this project.

# Example data

The `prox-exp*.json` files can be loaded as test/example data of a populated graph.

