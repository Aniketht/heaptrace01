# Mattersmith Knowledge

Repo for the work relating to Mattersmith | Knowledge and potentially future
developments for Andrew Scott.

## Project Structure

The current application is split across 3 services, supported by Neo4j
as a graph datastore.  Services interact via http, and are combined
through reverse-proxy nginx.

### Upload Service

In `/upload/`, a Python2+Flask web-application that handles user upload of files.

Python module requirements are in `/upload/requirements.txt`

Runs via `$ python -m uploadapi -- -api -noreload` (or Dockerfile with same)
TODO: should run under wsgi/gunicorn

By default listens on `0.0.0.0:5050`

Needs to be configured for expected `Host:` header, uploaded files storage dirs, ...


### Graph API

In `/webapi/`, a Python2+Flask web-app that provides all graph manipulation and retrieval interfaces to the frontend.

Requirements in `webapi/requirements.txt` (TODO: some might be unused/prunable)

Runs with `$ python -m webapi.webapi -- -api -noreload`

By default listens on `0.0.0.0:5000`

TODO: should run under wsgi/gunicorn

Also works running `gunicorn uploadapi:app`

### Frontend (Viz2)

In `/frontend/`, a jquery+cytoscape.js+others project for the client-side
code implementing UI and other parts of the system.

Built using webpack to a set of static files in `/frontend/dist` as part of the Dockerfile container startup process.

Currently complicated by some weird contortions to get a very slightly custom build of "Alpaca.js" to work properly with `npm` for building with webpack.

Build dependencies are variously in the Dockerfile build instructions, the `/frontend/package.json`, and Alpaca.js build requirements (currently out-of-tree)

Runs as an `nginx` Docker container that first compiles to static files, then execs nginx to serve those resources with some minor config modifications (setting `content-disposition: gzip` headers, CORS, etc)

Accepts some env variables at runtime to inform the frontend of the base URLs for the services it should connect to (API and upload-API)

Also works running `gunicorn opensecret.webapi:app`

### Glue Proxy

The 2 services and static files are brought together by an nginx reverse-proxy mapping url paths to the appropriate services.

This is also run in Docker, using dockerfile and nginx.conf in `/proxy/`

Config is quite basic: 

    location / { proxy_pass http://${VIZ2_CONTAINER_NAME}:80/; }
    location /api/ { proxy_pass http://${API_CONTAINER_NAME}:5000/; }
    location /upload/ { proxy_pass http://${UPLOADAPI_CONTAINER_NAME}:5050/; }


### Neo4j

Neo4j community edition (3.2.7) is run inside a docker container. The
base container image is the pre-built version supplied by Neo, with
some slight modifications.

A custom config file is required, and an additional procedure library
(`apoc-3.2.3.5-all.jar`) must be included.

During initial install the db needs to be bootstrapped (currently using a tarball copy of the db files in `neo4j/defaultdb.tgz` that should be extracted to the neo4j data directory.)

This sets a (bad) admin password, basic setup, etc.

It does not populate the DB with any of the necessary initial content (:Class/:RelationClass definitions, etc) necessary to actually use the application. This is done either by loading a `.cypher` snapshot from another DB, or calling the appropriate API or local python helper:

    # Cypher method
    # (generate)
    $ docker exec $NEO4J_CONTAINER bin/neo4j-shell -c dump > $DUMP_DEST
    # (load)
    $ docker exec $NEO4J_CONTAINER bin/neo4j-shell < $DUMP_DEST
    
    # HTTP API method
    # generated with GET /api/export/_graph > graphclean.json from a working instance
    $ http -j POST :5000/api/import/_graph < graphclean.json

    # Local python method
    $ python -m opensecret/fixture.py \
      -c 'destroy_and_refixture(); destroy_instances(); create_instances()'


## Configuration

The various services have some provisions for configuring things like
hostnames, ports, credentials but may not be 100% complete. Mostly
this is via environment variables passed to the services.

## Logging

Services are mostly configured to log to stdout/stderr to facilitate
log extraction from docker containers. While they mostly use python
`logging` module that could be configured for file storage, there are
probably some print() statements still lurking around.

## Security

The whole application is currently secured by placing it behind an
additional http reverse-proxy configured to require http basic auth
for all resources.

An active deployment should require TLS for any external access to
protect both the credentials and any actual data in the traffic.

## Storage and Backups

The frontend and webapi services are essentially stateless. The upload
service requires storage for the received files, and neo4j requires
persistent storage for the graphdb.

The uploadapi service currently just uses the filesystem without any
additional database or indices separate from it, so any consistent
filesystem snapshot should be acceptable as a backup.

The graphdb on the other hand requires either an online export/dump
procedure, or the orderly shutdown of the neo4j engine before the
filesystem objects are copied to ensure consistency.

We have in place some basic API & scripts for dumping graphdb
snapshots, which could be set up to periodically dump to another FS
location via cron and backed up as ordinary files from there.

## Internal Deployment

The application is currently both built and deployed using Docker containers.
The main orchestration/config is in docker-stack.yml and is deployed using docker swarm.
It uses named volumes for persistent storage for uploads and graphdb, and sets a lot of
the available configurtion options for hosts/ports/auth.

