#!/bin/bash
set -euo pipefail

export KNOWLEDGE_VERSION=$(git describe --tags --dirty --always)
export DIST_NAME="release-${KNOWLEDGE_VERSION}"
export DIST_TAR="${DIST_NAME}.tar.gz"

git checkout-index -a --prefix "${DIST_NAME}/" || true

pushd "${DIST_NAME}/"
rm -r "docs/internal/"
rm -r "ci/"
rm -r "experiments/" || true
rm -r "deployment/"

echo "${DIST_NAME}" > VERSION

echo "Making tar ${DIST_TAR}"
tar czvf "../$DIST_TAR" ./*
popd

rm -r "${DIST_NAME}/"

echo "Build tar ${DIST_TAR}"
