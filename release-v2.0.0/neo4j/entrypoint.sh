#!/bin/bash
set -euo pipefail


function pre_setup {
    echo "running pre_setup"
    tar -xvf /defaultdb.tgz -C /data
    mkdir -p /data/fixtures
    cp /fixtures/* /data/fixtures/
}

function post_setup {
    echo "running post_setup"
    sleep 2
    echo "testing if neo4j running..."
    while true; do
        if wget -O /dev/null http://localhost:7474/ ; then
            echo "installing fixtures..."
            /var/lib/neo4j/bin/neo4j-shell < /data/fixtures/initial_fixtures.cypher
            echo "fixtures done"
            break
        fi
        sleep 5
    done

    echo "done setup"
    touch /data/done_setup
}

echo "testing if done setup"
if [ ! -e /data/done_setup ]; then
    pre_setup
    post_setup &
fi

echo "running neo4j"
exec /var/lib/neo4j/bin/neo4j console

