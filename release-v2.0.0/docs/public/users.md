# User Management

User access control is currently implemented using HTTP Basic Auth on the
front-facing nginx statics/proxy server.

# Roles

There are currently 2 possible roles: `viewer` and `admin`. These are mutually
exclusive, and implemented simply as a suffix to the Basic Auth username.

All usernames should be of the form `$basename-$role`, for example
`testuser-viewer` or `tfeist-admin`. 

Users without, or with an invalid suffix will be treated as viewer role users,
but should be avoided.

# Testing

Credentials can be checked at the `/api/auth` endpoint. Invalid credentials will return a HTTP 401 response.
Success will return a 200 with JSON content. Only the `username` parameter in the response is significant. Other
values may not be indicative of the actual group/role, etc.

e.g.:

    $ http --auth $username:$passwd $INSTANCE_BASE_URL/api/auth
    
    
