# Running w/o docker Quickstart

Run Neo4j (developed/tested with 3.2.7) community edition, configure it with a username/password, custom conf and install the apoc plugin.
See current_deployment/neo4j/Dockerfile for how it's currently done.

Run the webapi with:	
    cd webapi
    virtualenv env
    source env/bin/activate
    pip install -r requirements.txt
    pip install gunicorn
    export OS_NEO4J_HTTP_URL="http://localhost:7474/db/data/"
    export OS_NEO4J_BOLT_URL="bolt://localhost:7687/db/data/"
    export OS_NEO4J_USERNAME=<neo4j username>
    export OS_NEO4J_PASSWORD=<neo4j password>
    gunicorn -b 127.0.0.1:8001 webapi:app
    gunicorn -b repindex.com:8001 webapi:app

Run the upload api with:

    cd uploadapi
    virtualenv env
    source env/bin/activate
    pip install -r requirements.txt
    pip install gunicorn
    export OPENSECRET_UPLOAD_FOLDER=/path/to/somewhere/to/store/uploads
    export OPENSECRET_UPLOAD_URL=https://my.host.name/upload/
    gunicorn -b 127.0.0.1:8002 upload:app

Run nginx
 - Configure it serve the static files at /.
 - Configure it to proxy /api* to the webapi (http://127.0.0.1:8001 in this example).
 - Configure it to proxy /upload* to the upload api (http://127.0.0.1:8002 in this example).
 - Add basic auth, SSL, gzip etc.

sudo http --timeout 500 --auth neo4j:admin -j POST http://127.0.0.1:8001/import/_graph < graphclean.json

sudo http --timeout 500 --auth neo4j:bacon4j -j POST http://127.0.0.1:5000/import/_graph < graphclean.json
Done!

