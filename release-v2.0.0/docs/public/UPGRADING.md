# Upgrading instructions

## v0.1a (opensecret-release-0.1a.tar.bz2)

Initial release. See install instructions.

## v2.0

1. Stop all services
2. Backup everything
 - neo4j database dump (see root README)
 - uploaded files
 - previous version sources
 - other system configuration (nginx.conf, etc)
3. Update uploadapi
4. Update webapi
 - note that python module paths have changed, from 'webapi/opensecret/webapi.py' to 'webapi/knowledge/webapi.py'
5. Update statics for frontend
 - unpack from statics/release-dist-*.tgz and move to location served by nginx
6. Update/check nginx config
 - nginx reverse-proxy should (in addition to performing the HTTP Basic auth checks),
   pass `Authorization:` headers to the webapi service. This can be tested as documented in docs/users.md#Testing
   As a fallback, the proxy can also pass an `x-remote-user:` header to the webAPI service, with nginx config like
   `add_header x-remote-user "$REMOTE_USER";` in the appropriate location sections.
   
7. Apply migrations to neo4j database
 - see migrations/README.md
8. Update httpasswd entries to $name-$role format
 - see docs/users.md
9. Restart services
10. Check for errors in log, verify app basic functions
 - non-authenticated users cannot reach either the static files or any proxied /api endpoints
 - authenticated user hitting `GET api/auth` contains the correct username
 - `GET /api/schema/graph` returns 200 with json body
 - `GET /api/graph` returns 200 with json body
 - `GET /upload/files` returns 200
 - visiting main site url in browser loads application
 
 
